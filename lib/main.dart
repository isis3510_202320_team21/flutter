import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:hive/hive.dart';
import 'package:teamup/firebase_options.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teamup/cubit/cubit_auth.dart';
import 'package:teamup/cubit/state_auth.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/repository/implementation/auth_repository.dart';
// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart' as path;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //Lugar donde se va a guardar el almacenamiento local
  final dir = await path.getApplicationDocumentsDirectory();
  // Se instancia la base de datos con su respectivo directorio
  Hive.init(dir.path);
  //Se inicializa la base de datos noSQL
  Hive.openBox("local_storage");
  Hive.openBox("register");
  //Initialization of firebaseapp
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  //Crashlytics event instance that will count the errors in the application
  await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  //Analytics event instance that counts the start of the application
  await FirebaseAnalytics.instance.logEvent(name: 'app_start');

  //Local Storage Instance
  await Hive.openBox("local_storage");
  await Hive.openBox("register");
  await Hive.openBox("filtered_won_matches");
  await Hive.openBox("filtered_lost_matches");
  WidgetsFlutterBinding.ensureInitialized();

  final authCubit = AuthCubit(AuthRepository());

  runApp(BlocProvider(
    create: (context) => authCubit..init(),
    child: MyApp.create(),
  ));
}

final _navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static Widget create() {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AuthSignedOut) {
          _navigatorKey.currentState
              ?.pushNamedAndRemoveUntil(Routes.login, (route) => false);
        } else if (state is AuthSignedIn) {
          _navigatorKey.currentState
              ?.pushNamedAndRemoveUntil(Routes.home, (route) => false);
        }
      },
      child: const MyApp(),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      navigatorKey: _navigatorKey,
      title: 'TeamUp',
      theme:
          ThemeData(scaffoldBackgroundColor: const Color.fromARGB(0, 5, 20, 1)),
      onGenerateRoute: Routes.routes,
      debugShowCheckedModeBanner: false,
    );
  }
}
