import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:teamup/database/shared_preferences.dart';
import 'package:teamup/model/matches.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// ignore: depend_on_referenced_packages

class MatchesProvider extends ChangeNotifier {
  //Se instancia  las shared prefs
  SharedPrefs prefs = SharedPrefs();
  //Se hacen las respectivas instanciaciones
  MatchesProvider();
  //Instancia de Firestore
  FirebaseFirestore get firestore => FirebaseFirestore.instance;
  //Instancia de Storage
  FirebaseStorage get storage => FirebaseStorage.instance;
  //Instance de Analytics
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  //Metodo para obtener los partidos de la comunidad
  Future<List<MyMatches>> getMatches() async {
    List<MyMatches> matches = [];
    try {
      final pla = await firestore.collection('matches').get();
      // ignore: avoid_function_literals_in_foreach_calls
      pla.docs.forEach((element) {
        return matches.add(MyMatches.fromFirebaseMap(element.data()));
      });
      return matches;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
      }
      return matches;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  //Metodo para guardar todos los places
  Future<void> addMatches() async {
    //Se inician las shared preferences
    prefs.sharePrefsInit();
    List<String> matches = await prefs.getMatches();
    //Se instancia el iterador
    int i = 0;
    //Se instancia el tamanio
    int size = matches.length;
    while (i < size) {
      //Se obtienen los atributos
      List<String> match = matches[i].split(',');
      String date = match[4];
      String place = "${match[2]},${match[3]}";
      String result = match[1];
      String team = match[0];
      String resultParsed = result.trimLeft();
      String dateParsed = date.trimLeft();
      String placeParsed = place.trimLeft();

      //Se crea el objeto
      MyMatches matchShare = MyMatches(
          date: dateParsed,
          place: placeParsed,
          result: resultParsed,
          team: team);
      //Se agrega a la instancia de base de datos
      await FirebaseFirestore.instance
          .collection("matches")
          .add(matchShare.toFirebaseMap());

      //Se itera
      i++;
    }
  }
}
