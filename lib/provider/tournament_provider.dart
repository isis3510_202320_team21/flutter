import 'dart:isolate';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart' as path;
import 'dart:async';
import 'package:hive/hive.dart';
import '../model/tournament.dart';

class TournamentProvider extends ChangeNotifier {
  Future<void> addAndSaveTournament(MyTournament tournament) async {
    final ref = firestore.doc('tournament/${tournament.name}');
    await ref.set(tournament.toFirebaseMap(), SetOptions(merge: true));
    notifyListeners();
  }

  void saveInFirebase(List<Object> args) async {
    SendPort sendPort = args[0] as SendPort;
    MyTournament tournament = args[1] as MyTournament;

    final ref = firestore.doc('tournament/${tournament.name}');
    await ref.set(tournament.toFirebaseMap(), SetOptions(merge: true));
    sendPort.send("Success");
  }

  FirebaseFirestore get firestore => FirebaseFirestore.instance;
  Future<List<MyTournament>> getTournaments() async {
    List<MyTournament> temp = [];
    try {
      final pla = await firestore.collection('tournament').get();

      for (var element in pla.docs) {
        MyTournament object = MyTournament.fromFirebaseMap(element.data());
        temp.add(object);
      }
      notifyListeners();
      saveInLocal(temp);
      return temp;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}:${e.message}');
      }
      notifyListeners();
      compute(saveInLocal, temp);
      return temp;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  void saveInLocal(List<MyTournament> tournaments) async {
    //Lugar donde se va a guardar el almacenamiento local
    final dir = await path.getApplicationDocumentsDirectory();
    // Se instancia la base de datos con su respectivo directorio
    Hive.init(dir.path);

    final box = await Hive.openBox('tournamentBox');
    for (int i = 0; i < tournaments.length; i++) {
      if (box.get(tournaments[i].name) == null) {
        String key = tournaments[i].name;
        String value =
            "${tournaments[i].name},${tournaments[i].sport},${tournaments[i].teams}";
        box.put(key, value);
      }
    }
  }

  Future<double> calculateDistance(lat1, lon1, lat2, lon2) async {
    ReceivePort port = ReceivePort();
    final isolate = await Isolate.spawn<List<dynamic>>(
          calculateDistanceIsolate, [port.sendPort, lat1, lon1, lat2, lon2]);
    final distance = await port.first;
    isolate.kill(priority: Isolate.immediate);
    return distance;
  }

  static void calculateDistanceIsolate(List<dynamic> values) {
    SendPort sendPort = values[0];
    double lat1 = values[1];
    double lon1 = values[2];
    double lat2 = values[3];
    double lon2 = values[4];
    var p = 0.017453292519943295;
    var a = 0.5 -
        cos((lat2 - lat1) * p) / 2 +
        cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2;
    double distance = 12742 * asin(sqrt(a));
    sendPort.send(distance);
  }

  

  Future<String> getText(distance) async {
    ReceivePort port = ReceivePort();
    final isolate = await Isolate.spawn<List<dynamic>>(
          getTextIsolate, [port.sendPort, distance]);
    final text = await port.first;
    isolate.kill(priority: Isolate.immediate);
    return text;
  }

  static void getTextIsolate(List<dynamic> values) {
    SendPort sendPort = values[0];
    double distance = values[1];
    Timestamp fecha = Timestamp.now();
    String formattedNumber = distance.toStringAsFixed(2);
    DateTime fecha2 = fecha.toDate();
    int difDias = fecha2.difference(DateTime.now()).inDays;
    String description =
        'You are at $formattedNumber Kilometer(s) from the Campus, be ready to travel and see your tournament. Good Luck!';
    if (distance > 10 && distance <= 100 && difDias == 0) {
      description =
          "You are at $formattedNumber Kilometers and the tournament is for today. Are you sure that you want to create this tournament?";
    } else if (distance > 100 && difDias == 0) {
      description =
          "It is not recommended to create a tournament when you can play with it. You are at $formattedNumber kilometers, are you sure taht you want to create this tournament?";
    } else if (distance > 100 && difDias > 0) {
      description = "You are at $formattedNumber kilometers";
    }
    sendPort.send(description);
  }
}
