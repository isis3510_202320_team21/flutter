import 'dart:isolate';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart' as path;
import 'dart:async';
import 'package:hive/hive.dart';
import '../model/sport.dart';

class SportProvider extends ChangeNotifier {
  Future<void> addAndSaveSport(MySport sport) async {
    final ref = firestore.doc('sport/${sport.name}');
    await ref.set(sport.toFirebaseMap(), SetOptions(merge: true));
    notifyListeners();
  }

  void saveInFirebase(List<Object> args) async {
    SendPort sendPort = args[0] as SendPort;
    MySport sport = args[1] as MySport;

    final ref = firestore.doc('sport/${sport.name}');
    await ref.set(sport.toFirebaseMap(), SetOptions(merge: true));
    sendPort.send("Success");
  }

  FirebaseFirestore get firestore => FirebaseFirestore.instance;
  Future<List<MySport>> getSports() async {
    List<MySport> temp = [];
    try {
      final pla = await firestore.collection('sport').get();

      for (var element in pla.docs) {
        MySport object = MySport.fromFirebaseMap(element.data());
        temp.add(object);
      }
      notifyListeners();
      compute(saveInLocal, temp);
      return temp;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}:${e.message}');
      }
      notifyListeners();
      //saveInLocal(temp);
      return temp;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  void saveInLocal(List<MySport> sports) async {
    //Lugar donde se va a guardar el almacenamiento local
    final dir = await path.getApplicationDocumentsDirectory();
    // Se instancia la base de datos con su respectivo directorio
    Hive.init(dir.path);

    final box = await Hive.openBox('SportBox');
    for (int i = 0; i < sports.length; i++) {
      if (box.get(sports[i].name) == null) {
        String key = sports[i].name;
        String value =
            "${sports[i].name},${sports[i].players},${sports[i].rules}";
        box.put(key, value);
      }
    }

  }
}
