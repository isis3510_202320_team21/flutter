import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:teamup/model/places.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import '../database/teamup_hive.dart';
// ignore: depend_on_referenced_packages

class PlacesProvider extends ChangeNotifier {
  //Se hacen las respectivas instanciaciones
  PlacesProvider();
  //Instancia de Firestore
  FirebaseFirestore get firestore => FirebaseFirestore.instance;
  //Instancia de Storage
  FirebaseStorage get storage => FirebaseStorage.instance;
  //Instance de Analytics
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
//Se instancia la db
  TeamUpHive localStorage = TeamUpHive();
  //Metodo para obtener los partidos de la comunidad
  Future<List<MyPlaces>> getPlaces() async {
    List<MyPlaces> places = [];
    try {
      final pla = await firestore.collection('places').get();
      // ignore: avoid_function_literals_in_foreach_calls
      pla.docs.forEach((element) {
        return places.add(MyPlaces.fromFirebaseMap(element.data()));
      });
      return places;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
      }
      return places;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  //Metodo para guardar todos los places
  Future<void> addPlaces() async {
    //Places
    List<String> places = localStorage.getPlaces();
    //Se instancia el iterador
    int i = 0;
    //Se instancia el tamanio
    int size = places.length;
    while (i < size) {
      //Se obtiene la location
      String locationActual = places[i];
      //Se crea el objeto
      MyPlaces place = MyPlaces(location: locationActual);
      //Se agrega a la base de datos
      //Se agrega a la instancia de base de datos
      await FirebaseFirestore.instance
          .collection("places")
          .add(place.toFirebaseMap());
      //Se itera
      i++;
    }
  }
}
