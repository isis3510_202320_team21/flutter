import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:teamup/model/routine.dart';

class RoutinesProvider extends ChangeNotifier {
  FirebaseFirestore get firestore => FirebaseFirestore.instance;

  Future<void> addToFireBase(MyRoutine routine) async {
    final ref = firestore.doc('routine/${routine.name}');
    await ref.set(routine.toFirebaseMap(), SetOptions(merge: true));
    notifyListeners();
  }

  Future<void> updateRateInFirebase(String routineName, int newRate) async {
    final ref = firestore.doc('routine/$routineName');
    await ref.update({'rate': newRate});
    await ref.update({'amount': FieldValue.increment(1)});
    notifyListeners();
  }

  Future<List<MyRoutine>> getSharedRoutines() async {
    List<MyRoutine> temp = [];
    try {
      final pla = await firestore.collection("routine").get();

      for (var element in pla.docs) {
        List<String> musc1 = element["muscles"].split(",");
        List<String> musc2 = element["secondaryMuscles"].split(",");
        List<String> equip = element["equipment"].split(",");
        List<String> img = <String>[element["image"].split(",")[0]];
        MyRoutine nuevo = MyRoutine(
            name: element["name"],
            description: element["description"],
            creationDate: element["creationDate"],
            category: element["category"],
            muscles: musc1,
            musclesSecondary: musc2,
            equipment: equip,
            images: img,
            comments: <String>[],
            author: element["author"],
            rate: element["rate"],
            amount: element["amount"]);
        temp.add(nuevo);
      }
      notifyListeners();
      return temp;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}:${e.message}');
      }
      notifyListeners();
      return temp;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<MyRoutine>> fetchExercisesByParameter(
      String list, Set<String> category, Set<String> leng) async {
    final response =
        await http.get(Uri.parse('https://wger.de/api/v2/exerciseinfo/'));
    List<MyRoutine> dataList = <MyRoutine>[];
    if (response.statusCode == 200) {
      final responseBody = response.body;
      Map<String, dynamic> jsonMap = json.decode(responseBody);
      saveMapAsTxt(jsonMap);
      List<dynamic> results = jsonMap["results"];
      int x = results.length;
      for (int i = 0; i < x; i++) {
        if (category.toString().contains(results[i]["category"]["name"]) &&
            leng.toString().contains(results[i]["language"]["full_name"])) {
          String data = results[i]["name"] + ":" + results[i]["description"];

          if (data.toLowerCase().contains(list.toLowerCase())) {
            List<dynamic> muscles = results[i]["muscles"];
            List<String> muscles2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              muscles2.add(muscles[k]["name"]);
            }
            List<dynamic> musclesSec = results[i]["muscles_secondary"];
            List<String> muscles2Sec = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              muscles2Sec.add(musclesSec[k]["name"]);
            }
            List<dynamic> equipment = results[i]["equipment"];
            List<String> equipment2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              equipment2.add(equipment[k]["name"]);
            }
            List<dynamic> images = results[i]["images"];
            List<String> images2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              images2.add(images[k]["image"]);
            }
            List<dynamic> comments = results[i]["comments"];
            List<String> comments2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              comments2.add(comments[k]["comment"]);
            }
            //print(results[i]["name"]);
            //crea la info de la rutina y la guarda
            MyRoutine rutina = MyRoutine(
                amount: 1,
                rate: 5,
                name: results[i]["name"],
                description: results[i]["description"],
                creationDate: results[i]["creation_date"],
                category: results[i]["category"]["name"],
                muscles: muscles2,
                musclesSecondary: muscles2Sec,
                equipment: equipment2,
                images: images2,
                comments: comments2,
                author: results[i]["license_author"]);
            dataList.add(rutina);
          }
        }
      }
      //print(dataList.length);
    } else {
      Map<String, dynamic> jsonMap = await readTxtAsMap();

      List<dynamic> results = jsonMap["results"];
      int x = results.length;
      for (int i = 0; i < x; i++) {
        if (category.toString().contains(results[i]["category"]["name"]) &&
            leng.toString().contains(results[i]["language"]["full_name"])) {
          String data = results[i]["name"] + ":" + results[i]["description"];

          if (data.toLowerCase().contains(list.toLowerCase())) {
            List<dynamic> muscles = results[i]["muscles"];
            List<String> muscles2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              muscles2.add(muscles[k]["name"]);
            }
            List<dynamic> musclesSec = results[i]["muscles_secondary"];
            List<String> muscles2Sec = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              muscles2Sec.add(musclesSec[k]["name"]);
            }
            List<dynamic> equipment = results[i]["equipment"];
            List<String> equipment2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              equipment2.add(equipment[k]["name"]);
            }
            List<dynamic> images = results[i]["images"];
            List<String> images2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              images2.add(images[k]["image"]);
            }
            List<dynamic> comments = results[i]["comments"];
            List<String> comments2 = <String>[];
            for (int k = 0; k < muscles.length; k++) {
              comments2.add(comments[k]["comment"]);
            }
            //print(results[i]["name"]);
            //crea la info de la rutina y la guarda
            MyRoutine rutina = MyRoutine(
                amount: 1,
                rate: 5,
                name: results[i]["name"],
                description: results[i]["description"],
                creationDate: results[i]["creation_date"],
                category: results[i]["category"]["name"],
                muscles: muscles2,
                musclesSecondary: muscles2Sec,
                equipment: equipment2,
                images: images2,
                comments: comments2,
                author: results[i]["license_author"]);
            dataList.add(rutina);
          }
        }
      }
    }
    notifyListeners();
    return dataList;
  }

  Future<Map<String, dynamic>> readTxtAsMap() async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/data.txt');

    if (await file.exists()) {
      final jsonString = await file.readAsString();
      final jsonData = jsonDecode(jsonString);
      if (jsonData is Map<String, dynamic>) {
        return jsonData;
      }
    }

    return {}; // Retorna un mapa vacío si no se puede leer el archivo o si no es un mapa válido.
  }

  Future<void> saveMapAsTxt(Map<String, dynamic> dataMap) async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/data.txt');

    final jsonString = jsonEncode(dataMap);

    await file.writeAsString(jsonString);
  }

  Future<List<MyRoutine>> fetchExercisesComplete(
      Set<String> category, Set<String> leng) async {
    final response =
        await http.get(Uri.parse('https://wger.de/api/v2/exerciseinfo/'));

    List<MyRoutine> dataList = <MyRoutine>[];
    if (response.statusCode == 200) {
      final responseBody = response.body;
      Map<String, dynamic> jsonMap = json.decode(responseBody);

      saveMapAsTxt(jsonMap);
      List<dynamic> results = jsonMap["results"];
      int x = results.length;
      for (int i = 0; i < x; i++) {
        if (category.toString().contains(results[i]["category"]["name"]) &&
            leng.toString().contains(results[i]["language"]["full_name"])) {
          //String data = results[i]["name"] + ":" + results[i]["description"];
          //print(data);

          List<dynamic> muscles = results[i]["muscles"];
          List<String> muscles2 = <String>[];
          if (muscles.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              muscles2.add(muscles[k]["name"]);
            }
          }

          List<dynamic> musclesSec = results[i]["muscles_secondary"];
          List<String> muscles2Sec = <String>[];
          if (musclesSec.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              muscles2Sec.add(musclesSec[k]["name"]);
            }
          }

          List<dynamic> equipment = results[i]["equipment"];
          List<String> equipment2 = <String>[];
          if (equipment.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              equipment2.add(equipment[k]["name"]);
            }
          }

          List<dynamic> images = results[i]["images"];
          List<String> images2 = <String>[];
          if (images.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              images2.add(images[k]["image"]);
            }
          }

          List<dynamic> comments = results[i]["comments"];
          List<String> comments2 = <String>[];
          if (comments.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              comments2.add(comments[k]["comment"]);
            }
          }

          //print(results[i]["name"]);
          //crea la info de la rutina y la guarda

          MyRoutine rutina = MyRoutine(
              amount: 1,
              rate: 5,
              name: results[i]["name"],
              description: results[i]["description"],
              creationDate: results[i]["creation_date"],
              category: results[i]["category"]["name"],
              muscles: muscles2,
              musclesSecondary: muscles2Sec,
              equipment: equipment2,
              images: images2,
              comments: comments2,
              author: results[i]["license_author"]);

          dataList.add(rutina);
        }
      }
      //print(dataList.length);
    } else {
      Map<String, dynamic> jsonMap = await readTxtAsMap();

      List<dynamic> results = jsonMap["results"];
      int x = results.length;
      for (int i = 0; i < x; i++) {
        if (category.toString().contains(results[i]["category"]["name"]) &&
            leng.toString().contains(results[i]["language"]["full_name"])) {
          //String data = results[i]["name"] + ":" + results[i]["description"];
          //print(data);

          List<dynamic> muscles = results[i]["muscles"];
          List<String> muscles2 = <String>[];
          if (muscles.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              muscles2.add(muscles[k]["name"]);
            }
          }

          List<dynamic> musclesSec = results[i]["muscles_secondary"];
          List<String> muscles2Sec = <String>[];
          if (musclesSec.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              muscles2Sec.add(musclesSec[k]["name"]);
            }
          }

          List<dynamic> equipment = results[i]["equipment"];
          List<String> equipment2 = <String>[];
          if (equipment.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              equipment2.add(equipment[k]["name"]);
            }
          }

          List<dynamic> images = results[i]["images"];
          List<String> images2 = <String>[];
          if (images.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              images2.add(images[k]["image"]);
            }
          }

          List<dynamic> comments = results[i]["comments"];
          List<String> comments2 = <String>[];
          if (comments.isNotEmpty) {
            for (int k = 0; k < muscles.length; k++) {
              comments2.add(comments[k]["comment"]);
            }
          }

          //print(results[i]["name"]);
          //crea la info de la rutina y la guarda

          MyRoutine rutina = MyRoutine(
              amount: 1,
              rate: 5,
              name: results[i]["name"],
              description: results[i]["description"],
              creationDate: results[i]["creation_date"],
              category: results[i]["category"]["name"],
              muscles: muscles2,
              musclesSecondary: muscles2Sec,
              equipment: equipment2,
              images: images2,
              comments: comments2,
              author: results[i]["license_author"]);

          dataList.add(rutina);
        }
      }
    }
    return dataList;
  }

  Future<List<String>> getCategories() async {
    final response =
        await http.get(Uri.parse('https://wger.de/api/v2/exercisecategory/'));
    List<String> dataList = <String>[];
    if (response.statusCode == 200) {
      // La solicitud fue exitosa, puedes procesar los datos aquí
      final responseBody = response.body;

      Map<String, dynamic> jsonMap = json.decode(responseBody);
      List<dynamic> results = jsonMap['results'];
      int x = results.length;
      for (int i = 0; i < x; i++) {
        String data = results[i]['name'];
        dataList.add(data);
      }

      // Imprimir la lista de strings
      //print(dataList.length);
      //print(dataList);
      //print(responseBody);
    } else {
      // La solicitud no fue exitosa, maneja el error aquí
    }
    notifyListeners();
    return dataList;
  }
}
