import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static SharedPreferences? _sharedPreferences;
//Se obtiene la instancia de las shared Preferences
  sharePrefsInit() async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
  }

//Almacena localmente la información del partido
  setMatchInformation(String match) async {
    // Se determina el id a guardar del partido
    int size = await getMatchSize();
    if (size > 0) {
      //Se actualiza el tamanio del arreglo
      int id = await setMatchSize(size);
      //Se almacenan los datos
      await _sharedPreferences?.setString('$id', match);
    } else {
      //Se actualiza el tamanio del arreglo a 1
      int id = await setMatchSize(0);
      //Se guarda el partido
      await _sharedPreferences?.setString('$id', match);
    }
  }

//Lleva la cuenta del tamanio de elementos
  setMatchSize(int value) async {
    //Se agrega 1 a la cuenta
    int id = value + 1;
    //Se almacena la cantidad
    await _sharedPreferences?.setInt("size", id);
    return id;
  }

//Metodo que devuelve retorna el tamanio de elementos
  Future<int> getMatchSize() async {
    return _sharedPreferences?.getInt("size") ?? 0;
  }

  //Metodo que devuelve la infromación de un partido específico
  Future<String?> getMatchInformation(int id) async {
    return _sharedPreferences?.getString('$id');
  }

  //Metodo que retorna los partidos en forma de lista
  Future<List<String>> getMatches() async {
    //Iterador para el bucle
    int i = 1;
    //Tamanio de elementos en el arreglo
    int size = await getMatchSize();
    //Declaracion de arreglo donde se van a almacenar los partidos
    List<String> matches = [];
    //print(size);
    //var keys = await _sharedPreferences?.getKeys();
    //print(keys);

    while (i <= size) {
      //Se obtiene el partido
      String? match = await getMatchInformation(i);
      //Se agrega el partido a la lista
      matches.add(match!);
      //Se incrementa el iterador y sigue el bucle
      i++;
    }
    return matches;
  }

  //Metodo para retornar los partidos con win
  Future<List<String>> getMatchesWon() async {
    //Iterador para el bucle
    int i = 1;
    //Tamanio de elementos en el arreglo
    int size = await getMatchSize();
    //Declaracion de arreglo donde se van a almacenar los partidos
    List<String> matches = [];
    //Se agrega el primero
    while (i <= size) {
      //Se obtiene el partido
      String? match = await getMatchInformation(i);
      List<String> matchParsed = match!.split(',');
      String result = matchParsed[1];
      if (result.startsWith(' W') == true) {
        //Se agrega el partido a la lista
        matches.add(match);
      }
      //Se incrementa el iterador y sigue el bucle
      i++;
    }
    return matches;
  }

  //Metodo para retornar los partidos con Loss
  Future<List<String>> getMatchesLost() async {
    //Iterador para el bucle
    int i = 1;
    //Tamanio de elementos en el arreglo
    int size = await getMatchSize();
    //Declaracion de arreglo donde se van a almacenar los partidos
    List<String> matches = [];
    //Se agrega el primero
    while (i <= size) {
      //Se obtiene el partido

      String? match = await getMatchInformation(i);
      List<String> matchParsed = match!.split(',');
      String result = matchParsed[1];
      if (result.startsWith(' L') == true) {
        //Se agrega el partido a la lista
        matches.add(match);
      }
      //Se incrementa el iterador y sigue el bucle
      i++;
    }
    return matches;
  }

  //Metodo que ajusta la id de cada uno de los partidos acorde al tamanio
  Future<bool?> deleteMatch(int matchId) async {
    //Se declara el iterador 1
    bool? deleted = false;
    int i = 1;
    int size = await getMatchSize();
    while (i <= size) {
      if (matchId == i) {
        //Se elimina el partido y se ajusta el tamanio
        deleted = await _sharedPreferences?.remove('$matchId');
        await setMatchSize(size - 1);
      } else if (i > matchId) {
        //Se obtiene la informacion del partido actual
        // ignore: await_only_futures
        String? matchActual = await _sharedPreferences?.getString('$i');
        //Se actualiza su id
        int nuevoId = i - 1;
        //Se actualiza los matches
        await _sharedPreferences?.setString('$nuevoId', matchActual!);
      }
      i++;
    }
    return deleted;
  }

//Metodo encargado de eliminar todos los partidos
  Future<bool?> deleteMatches() async {
    bool rta = false;
    //Iterador para el bucle
    int i = 1;
    //Tamanio de elementos en el arreglo
    int size = await getMatchSize();
    //Declaracion de arreglo donde se van a almacenar los partidos
    //Se agrega el primero
    while (i < size) {
      //Se Elimina el partido x
      bool? rta = await deleteMatch(i);
      if (rta == false) {
        //Si hubo un problema borrando alguno de los partidos se sale del bucle
        i = size;
      }
      //Se incrementa el iterador y sigue el bucle
      i++;
    }
    return rta;
  }

  //Set matches with most wins
  //Lleva la cuenta del tamanio de elementos
  Future<bool?> setDateMostWins(String date, int wins) async {
    //Se almacena la cantidad
    return await _sharedPreferences?.setInt(date, wins);
  }

//Metodo que obtiene el numero de victorias
  Future<int?> getDateMostWins(String date) async {
    return _sharedPreferences?.getInt(date);
  }

  //Set matches with most wins
  //Lleva la cuenta del tamanio de elementos
  Future<bool?> setDateMostLost(String date, int lost) async {
    //Se almacena la cantidad
    return await _sharedPreferences?.setInt('${date}l', lost);
  }

//Metodo que obtiene el numero de victorias
  Future<int?> getDateMostLost(String date) async {
    return _sharedPreferences?.getInt('${date}l');
  }
}
