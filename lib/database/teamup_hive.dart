import 'package:hive/hive.dart';

class TeamUpHive {
  //Caja que usaremos como variable
  late Box mapBox;

//Sets a place with
  setPlace(String location) {
    mapBox = Hive.box("local_storage");
    mapBox.add(location);
  }

//Sets a filtered match
  setMatchesWon(date, number) {
    mapBox = Hive.box("filtered_won_matches");
    mapBox.put(date, number);
  }

  //Get matches won
  //Returns a list with the places saved locally
  List<String> getMatchesWon() {
    //Se abre la caja
    mapBox = Hive.box("filtered_won_matches");
    //Se obtiene el tamaño de la caja
    int size = mapBox.length;
    //Declaracion de iterador
    int i = 0;
    List<String> matches = [];
    //Inicio del loop
    while (i < size) {
      //Se continua con el loop
      String matchActual = mapBox.getAt(i);
      //Eliminar duplicados
      matches.add(matchActual);
      i++;
    }
    return matches;
  }

  //Returns a list with the places saved locally
  List<String> getPlaces() {
    //Se abre la caja
    mapBox = Hive.box("local_storage");
    //Se obtiene el tamaño de la caja
    int size = mapBox.length;
    //Declaracion de iterador
    int i = 0;
    List<String> places = [];
    //Inicio del loop
    while (i < size) {
      //Se continua con el loop
      String locationActual = mapBox.getAt(i);
      //Eliminar duplicados
      places.add(locationActual);
      i++;
    }
    return places;
  }

//Verifies if the place was created before
  bool placeCreated(String place) {
    bool duplicado = false;
    //Se abre la caja
    mapBox = Hive.box("local_storage");
    //Se obtiene el tamanio de la caja
    List<String> places = getPlaces();
    //Declaracion de iterador
    int i = 0;
    //Inicio del loop
    while (i < places.length) {
      //Se obtiene la ubicacion i
      String placeActual = mapBox.getAt(i);
      //Se verifica si son iguales
      if (placeActual == place) {
        //Se declara que es es un duplicado
        duplicado = true;
        //Se termina el loop
        i = places.length;
      }
      i++;
    }
    return duplicado;
  }

  void deletePlaces() {
    //Se abre la caja
    mapBox = Hive.box("local_storage");
    //Se elminia el contenido de la caja
    mapBox.clear();
  }
}
