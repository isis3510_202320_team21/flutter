import 'package:flutter/material.dart';
import 'package:teamup/screen/create_tournament_screen.dart';
import 'package:teamup/screen/community_matches_screen.dart';
import 'package:teamup/screen/community_places_screen.dart';
import 'package:teamup/screen/daily_video_screen.dart';
import 'package:teamup/screen/login_screen.dart';
import 'package:teamup/screen/charging_screen.dart';
import 'package:teamup/screen/map_screen.dart';
import 'package:teamup/screen/matches_screen.dart';
import 'package:teamup/screen/motivating_screen.dart';
import 'package:teamup/screen/register_screen.dart';
import 'package:teamup/screen/home_screen.dart';
import 'package:teamup/screen/routines_screen.dart';
import 'package:teamup/screen/settings_screen.dart';
import 'package:teamup/screen/teams_screen.dart';
import 'package:teamup/screen/create_team_screen.dart';
import 'package:teamup/screen/sports_screen.dart';
import 'package:teamup/screen/tournament_screen.dart';
import 'package:teamup/screen/sharedRout_screen.dart';

import '../screen/match_screen.dart';
import '../screen/places_screen.dart';
import '../screen/track_screen.dart';
import '../screen/weather_screen.dart';

class Routes {
  static const charging = '/';
  static const login = '/login';
  static const profile = '/profile';
  static const home = '/home';
  static const register = '/register';
  static const settings = '/settings';
  static const teams = '/teams';
  static const createTeam = '/createteam';
  static const createTournament = '/createtournament';
  static const map = '/map';
  static const weather = '/weather';
  static const routines = '/routines';
  static const tracking = '/track';
  static const places = '/places';
  static const match = '/match';
  static const matches = '/matches';
  static const sports = '/sports';
  static const communityMatches = '/communityMatches';
  static const communityPlaces = '/communityPlaces';
  static const tournaments = '/tournaments';
  static const sharedRoutines = "/sharedRoutines";
  static const dailyVid = 'dailyVideo';
  static const motivatingVid = 'motivatingVideo';

  static Route routes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case login:
        return _buildRoute(LoginScreen.create);
      case charging:
        return _buildRoute(SplashScreen.create);
      case register:
        return _buildRoute(RegisterScreen.create);
      case home:
        return _buildRoute(HomeScreen.create);
      case settings:
        return _buildRoute(SettingsScreen.create);
      case teams:
        return _buildRoute(TeamsScreen.create);
      case createTeam:
        return _buildRoute(CreateTeamScreen.create);
      case map:
        return _buildRoute(MapScreen.create);
      case weather:
        return _buildRoute(WeatherScreen.create);
      case routines:
        return _buildRoute(RoutinesScreen.create);
      case tracking:
        return _buildRoute(TrackScreen.create);
      case places:
        return _buildRoute(PlacesScreen.create);
      case match:
        return _buildRoute(MatchScreen.create);
      case matches:
        return _buildRoute(MatchesScreen.create);
      case sports:
        return _buildRoute(SportScreen.create);
      case tournaments:
        return _buildRoute(TournamentScreen.create);
      case createTournament:
        return _buildRoute(CreateTournamentScreen.create);
      case communityMatches:
        return _buildRoute(CommunityMatchesScreen.create);
      case sharedRoutines:
        return _buildRoute(ListRoutine_screen.create);
      case communityPlaces:
        return _buildRoute(CommunityPlacesScreen.create);
      case dailyVid:
        return _buildRoute(DailyVideoScreen.create);
      case motivatingVid:
        return _buildRoute(MotivatingScreen.create);
      default:
        throw Exception('Route does not exist');
    }
  }

  static MaterialPageRoute _buildRoute(Function build) =>
      MaterialPageRoute(builder: (context) => build(context));
}
