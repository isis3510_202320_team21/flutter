import 'package:flutter/material.dart';

ImageIcon piano = const ImageIcon(
  AssetImage('piano.png'),
  color: Color(0xFF3A5A98),
);