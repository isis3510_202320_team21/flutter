import 'dart:convert';
import 'dart:io';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import '../model/weather.dart';

//Variables de Url
String city = 'bogota';
String apiKey = "665205fd74b7ffc328e72636328a9664";
//Url del servicio de transmilenio
// ignore: non_constant_identifier_names
String TRANSMI_URL =
    "https://gis.transmilenio.gov.co/arcgis/rest/services/Troncal/consulta_estaciones_troncales/FeatureServer/1/query?where=1%3D1&outFields=numero_estacion,nombre_estacion,coordenada_x_estacion,coordenada_y_estacion,ubicacion_estacion,troncal_estacion,numero_vagones_estacion,numero_accesos_estacion,biciestacion_estacion,capacidad_biciestacion_estacion,tipo_estacion,biciparqueadero_estacion,latitud_estacion,longitud_estacion&outSR=4326&f=json";
//Url del servicio de weather
// ignore: non_constant_identifier_names
String BOGOTA_URL =
    "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey&units=metric";

class ServiceBroker {
  //Se declaran variables que otorgan acceso a la ubicacion
  // ignore: non_constant_identifier_names
  late double latitude_cache;
  // ignore: non_constant_identifier_names
  late double longitude_cache;
  //Metodo que hace el API Request del Transmilenio
  Future<String> getEstaciones() async {
    final file = await getEstacionesCaching();
    //Read the file
    final contents = file.readAsString();

    return contents;
  }

  //Metodo que hace el API request del Clima
  Future<Weather>? getCurrentWeather() async {
    //Api request and cache
    final file = await getCurrentWeatherCaching();
    //Read the file
    String contents = await file.readAsString();
    return Weather.fromJson(jsonDecode(contents));
  }

  //Metodo que hace el Api request del clima con la ubicacion actual
  Future<Weather> getCurrentWeatherFromLocation(latitude, longitude) async {
    //Api request and cache
    final file =
        await getCurrentWeatherFromLocationCaching(latitude, longitude);
    //Read the file
    String contents = await file.readAsString();
    return Weather.fromJson(jsonDecode(contents));
  }

  //Metodo que guarda en cache cada uno de los responses por http
  setCaching(url) async {
    await DefaultCacheManager().downloadFile(url);
  }

  //Revisa si las estaciones están en cache  y si no hace el response
  Future<File> getEstacionesCaching() async {
    var file = await DefaultCacheManager().getSingleFile(TRANSMI_URL);

    return File(file.path);
  }

  //Revisa si el clima de bogotá está en cache y si no hace el response
  Future<File> getCurrentWeatherCaching() async {
    var file = await DefaultCacheManager().getSingleFile(BOGOTA_URL);

    return File(file.path);
  }

  //Revisa si el clima de bogotá está en cache
  Future<File> getCurrentWeatherFromLocationCaching(latitude, longitude) async {
    var url =
        'https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude&appid=$apiKey';
    var file = await DefaultCacheManager().getSingleFile(url);
    return File(file.path);
  }

//Elimina del cache la información de las estaciones
  // ignore: unused_element
  void _removeWeatherFromLocationCaching() async {
    // ignore: unused_local_variable
    var url =
        'https://api.openweathermap.org/data/2.5/weather?lat=$latitude_cache&lon=$longitude_cache&appid=$apiKey';
    await DefaultCacheManager().removeFile(BOGOTA_URL).then((value) {
      // ignore: avoid_print
      print('Current Weather Caching removed');
    }).onError((error, stackTrace) {
      //ignore: avoid_print
      print(error);
    });
  }
// Elimina el cache del weather con ubicacion
}
