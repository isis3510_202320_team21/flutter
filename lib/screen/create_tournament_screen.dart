import 'package:blurry/blurry.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/tournament.dart';
import 'package:teamup/navigation/routes.dart';

import '../provider/tournament_provider.dart';

class CreateTournamentScreen extends StatefulWidget {
  const CreateTournamentScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: TournamentProvider())],
        child: const CreateTournamentScreen());
  }

  @override
  State<StatefulWidget> createState() => _CreateTournamentScreenState();
}

class _CreateTournamentScreenState extends State<CreateTournamentScreen> {
  FlutterTts flutterTts = FlutterTts();
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final amountController = TextEditingController();
  String nameValue = "";
  int amountValue = 0;
  Timestamp fecha = Timestamp.now();
  Timestamp base = Timestamp.now();

  List<String> optionsSports = [
    "soccer",
    "micro-soccer",
    "beach volleyball",
    "volleyball",
    "Basketball",
    "ping pong"
  ];
  String sportSelected = "soccer";

  GoogleMapController? mapController; //contrller for Google map
  PolylinePoints polylinePoints = PolylinePoints();

  String googleAPiKey = "AIzaSyBcaGpXra3aoAiPhtBxQJ2Zpxhhb9UHSeA";

  Set<Marker> markers = {}; //markers for google map
  Map<PolylineId, Polyline> polylines = {}; //polylines to show direction

  LatLng startLocation = const LatLng(4.60296, -74.06521);
  LatLng endLocation = const LatLng(4.60296, -74.06521);

  double distance = 5.0;

  String text = "";

  Future<LatLng?> _determinePosition() async {
    LocationData? currentLocation;
    var location = Location();
    try {
      // Find and store your location in a variable
      currentLocation = await location.getLocation();
      double latitude = currentLocation.latitude!;
      double longitude = currentLocation.longitude!;
      // Se mueve la camara a la posicion encontrada con el controlador
      LatLng temp = LatLng(latitude, longitude);
      return temp;
    } on Exception {
      currentLocation = null;
    }
    return null;
  }

  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  void buildPopUp(BuildContext context, String text) {
    Blurry.info(
      title: 'Creating tournament',
      description: text,
      confirmButtonText: 'Create',
      onConfirmButtonPressed: () {
        Provider.of<TournamentProvider>(context, listen: false)
            .addAndSaveTournament(MyTournament(
                name: nameValue,
                sport: sportSelected,
                teams: amountValue,))
            .then((_) {
          Navigator.pushNamed(context, Routes.tournaments);
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Tournament created!'),
            ),
          );
        });
      },
    ).show(context);
  }

  Future getDistance(BuildContext context){
    return Provider.of<TournamentProvider>(context, listen: false).calculateDistance(
                                      startLocation.latitude,
                                      startLocation.longitude,
                                      endLocation.latitude,
                                      endLocation.longitude);
  }

  Future getText(BuildContext context, double distance){
    return Provider.of<TournamentProvider>(context, listen: false).getText(distance);
  }

  String? nameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "This is a required field";
    }
    return null;
  }

  String? amountValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "This is a required field";
    } else {
      int? value2 = int.tryParse(value);
      if (value2 == null) {
        return "This field must be a numeric value";
      } else {
        if (value2 > 64) {
          return "The member limit is 22 for the available sports";
        } else if (value2 < 2) {
          return "You can't create a tournament with 0 or 1 members";
        }
      }
    }
    return null;
  }

  String? dropTableValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Please select an option";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 0, 0),
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        actions: [
          IconButton(
              onPressed: () => Navigator.pushNamed(context, Routes.settings),
              icon: const Icon(Icons.list),
              color: Colors.white),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Form(
            key: formKey,
            child: ListView(
              children: [
                const SizedBox(
                  height: 16,
                ),
                TextFormField(
                  validator: nameValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: nameController,
                  maxLength: 30,
                  decoration: const InputDecoration(
                      labelText: 'Tournament name',
                      filled: true,
                      fillColor: Colors.white,
                      icon:
                          Icon(Icons.spellcheck_rounded, color: Colors.white)),
                  style: const TextStyle(color: Colors.black),
                  onChanged: (value) {
                    setState(() {
                      nameValue = value;
                    });
                  },
                ),
                const SizedBox(
                  height: 8,
                ),
                TextFormField(
                  validator: amountValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: amountController,
                  maxLength: 30,
                  decoration: const InputDecoration(
                      labelText: '# of teams',
                      filled: true,
                      fillColor: Colors.white,
                      icon:
                          Icon(Icons.spellcheck_rounded, color: Colors.white)),
                  style: const TextStyle(color: Colors.black),
                  onChanged: (value) {
                    setState(() {
                      amountValue = int.parse(value);
                    });
                  },
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(
                  height: 8,
                ),
                DropdownButtonFormField(
                    validator: dropTableValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        filled: true, fillColor: Colors.white),
                    hint: const Text("Select one sport"),
                    items: optionsSports.map((e) {
                      return DropdownMenuItem(
                        value: e,
                        child: Text(e),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      if (value != null) sportSelected = value;
                    }),
                const SizedBox(
                  height: 24,
                ),

                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () async {
                    Stream<bool> xd = Stream.fromFuture(messageNet(context));
                    // ignore: unrelated_type_equality_checks
                    if (xd == true) {
                      if (formKey.currentState?.validate() == true) {
                        Future<LatLng?> futuro = _determinePosition();
                        futuro.then((value) async => {
                              if (value != null)
                                {
                                  startLocation = value,
                                  distance = await getDistance(context),
                                  text = await getText(context, distance),
                                  buildPopUp(context, text)
                                }
                            });
                      }
                    } else {
                      // ignore: use_build_context_synchronously
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('You are offline, try again later'),
                        ),
                      );
                    }
                  },
                  child: const Text('Create tournament'),
                ),
              ],
            )),
      ),
    );
  }
}
