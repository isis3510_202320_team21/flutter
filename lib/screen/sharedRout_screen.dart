// ignore_for_file: file_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/routine.dart';
import 'package:teamup/provider/routines_provider.dart';

// ignore: camel_case_types
class ListRoutine_screen extends StatefulWidget {
  const ListRoutine_screen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: RoutinesProvider())],
        child: const ListRoutine_screen());
  }

  @override
  State<StatefulWidget> createState() => _ListRout_ScreenState();
}

// ignore: camel_case_types
class _ListRout_ScreenState extends State<ListRoutine_screen> {
  List<MyRoutine> listaP = List.empty();

  void asignarLista(BuildContext context) {
    Future<List<MyRoutine>> lista =
        Provider.of<RoutinesProvider>(context).getSharedRoutines();
    lista.then((value) => listaP = value);
  }

  @override
  Widget build(BuildContext context) {
    asignarLista(context);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
      body: Column(
        children: [
          const SizedBox(
            height: 90,
            width: double.infinity,
            child: RepaintBoundary(
                child: Text(
              'Rate the routines here!',
              textAlign: TextAlign.center,
              textScaleFactor: 2.5,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            )),
          ),
          buildList(context)
        ],
      ),
    );
  }

  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  Widget buildList(BuildContext context) {
    if (listaP.isEmpty) {
      Widget t = Container();
      Stream<bool> mensaje = Stream.fromFuture(messageNet(context));
      // ignore: unrelated_type_equality_checks
      if (mensaje == false) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Check your internet connection'),
          backgroundColor: Color.fromRGBO(229, 36, 55, 1),
        ));
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Loading routines...",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            )
          ],
        );
      } else {
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Check your internet connection... ",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 40, // Establece el ancho del indicador
              height: 40, // Establece la altura del indicador
              child: CircularProgressIndicator(
                strokeWidth: 4, // Cambia el tamaño del indicador
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.blue), // Cambia el color del indicador
              ),
            )
          ],
        );
      }
      return t;
    } else {
      return Expanded(
          child: ListView.builder(
              itemCount: listaP.length,
              itemBuilder: ((context, index) {
                final item = listaP[index];
                return buildBlock(context, item);
              })));
    }
  }

  Widget buildStarWidget2(MyRoutine rout) {
    List<Widget> stars = <Widget>[];
    for (int i = 0; i < 5; i++) {
      stars.add(const Icon(Icons.star_border, size: 30, color: Colors.yellow));
    }
    for (int i = 0; i < rout.rate; i++) {
      stars[i] = const Icon(Icons.star, size: 30, color: Colors.yellow);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {},
              child: stars[0],
            ),
            const SizedBox(
              width: 05,
            ),
            GestureDetector(
              onTap: () {},
              child: stars[1],
            ),
            const SizedBox(
              width: 05,
            ),
            GestureDetector(
              onTap: () {},
              child: stars[2],
            ),
            const SizedBox(
              width: 05,
            ),
            GestureDetector(
              onTap: () {},
              child: stars[3],
            ),
            const SizedBox(
              width: 05,
            ),
            GestureDetector(
              onTap: () {},
              child: stars[4],
            ),
          ],
        ),
      ],
    );
  }

  Widget buildBlock(BuildContext context, MyRoutine routine) {
    String media;
    if (routine.images.isEmpty) {
      media = "https://wger.de/media/exercise-images/91/Crunches-1.png";
    } else {
      media = routine.images[0];
    }
    String muscles = "Not specified";
    if (routine.muscles.isNotEmpty) {
      muscles =
          routine.muscles.toString().replaceAll("[", "").replaceAll("]", "");
      if (muscles == "") {
        muscles = "Not specified";
      }
    }
    String secMuscles = "Not specified";
    if (routine.musclesSecondary.isNotEmpty) {
      secMuscles = routine.musclesSecondary
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
      if (secMuscles == "") {
        secMuscles = "Not specified";
      }
    }
    String equipment = "Not specified";
    if (routine.equipment.isNotEmpty) {
      equipment =
          routine.equipment.toString().replaceAll("[", "").replaceAll("]", "");
      if (equipment == "") {
        equipment = "Not specified";
      }
    }

    return Container(
      color: Colors.black,
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          color: const Color.fromARGB(255, 98, 98, 98),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // header
              Container(
                padding: const EdgeInsetsDirectional.only(
                    start: 20, end: 20, top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              routine.author,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.white,
                              ),
                            ),
                            RichText(
                              text: const TextSpan(
                                children: [
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.location_on_outlined,
                                      size: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "USA",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              // content
              GestureDetector(
                onDoubleTap: () {
                  // ignore: todo
                  // TODO: show like effect
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Stack(
                    children: [
                      const SizedBox(
                        width: 700,
                        height: 370,
                      ),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(35),
                          child: SizedBox(
                            width: 700,
                            height: 350,
                            child: CachedNetworkImage(
                              imageUrl: media,
                              fit: BoxFit.cover,
                              placeholder: (context, url) =>
                                  const CircularProgressIndicator(
                                strokeWidth: 4,
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.blue),
                              ),
                              errorWidget: (context, url, error) => const Icon(
                                Icons.error,
                                color: Colors.red,
                              ),
                            ),
                          )),
                      Positioned(
                        bottom: 300,
                        left: 50,
                        right: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            height: 60,
                            color: Colors.black,
                            child: buildStarWidget2(routine),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 50,
                        right: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Container(
                            height: 50,
                            color: Colors.black,
                            child: ChangeNotifierProvider.value(
                              value:
                                  RoutinesProvider(), // Proporcionar la instancia del proveedor
                              child: StarWidget(
                                  routine:
                                      routine), // Pasar la instancia del proveedor al widget hijo
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              const SizedBox(
                height: 5,
              ),

              // // actions
              // const PostActionsWidget(),
              // likes
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "${routine.name}: "),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "Category: ${routine.category} "),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "created ${routine.creationDate}"),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Description",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              // description
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: routine.description,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Muscles:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: muscles,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Secondary muscles:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: secMuscles,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      // hashtags
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Equipment:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: equipment,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              // comments
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: const Text(
                  'texto 7',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class StarWidget extends StatefulWidget {
  final MyRoutine routine;

  const StarWidget({super.key, required this.routine});

  @override
  // ignore: library_private_types_in_public_api
  _StarWidgetState createState() => _StarWidgetState();
}

class _StarWidgetState extends State<StarWidget> {
  List<Widget> stars = <Widget>[
    const Icon(Icons.star_border, size: 30, color: Colors.white),
    const Icon(Icons.star_border, size: 30, color: Colors.white),
    const Icon(Icons.star_border, size: 30, color: Colors.white),
    const Icon(Icons.star_border, size: 30, color: Colors.white),
    const Icon(Icons.star_border, size: 30, color: Colors.white),
  ];

  String name = "";
  int amount = 0;
  int actualRate = 0;
  @override
  void initState() {
    super.initState();
    name = widget.routine.name;
    amount = widget.routine.amount;
    actualRate = widget.routine.rate;
  }

  void changeRate(BuildContext context, int newRate) {
    int rate = (((actualRate * amount) + newRate) / (amount + 1)).ceil();

    final routinesProvider = Provider.of<RoutinesProvider>(context,
        listen: false); // Obtener la instancia del proveedor

    routinesProvider.updateRateInFirebase(
        name, rate); // Llamar al método del proveedor
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: RoutinesProvider())],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    changeRate(context, 1);
                    setState(() {
                      stars[0] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      // También puedes actualizar las estrellas anteriores
                      for (int i = 1; i < stars.length; i++) {
                        stars[i] = const Icon(Icons.star_border,
                            size: 30, color: Colors.white);
                      }
                    });
                  },
                  child: stars[0],
                ),
                const SizedBox(width: 05),
                GestureDetector(
                  onTap: () {
                    changeRate(context, 2);
                    setState(() {
                      stars[0] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[1] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      // También puedes actualizar las estrellas anteriores
                      for (int i = 2; i < stars.length; i++) {
                        stars[i] = const Icon(Icons.star_border,
                            size: 30, color: Colors.white);
                      }
                    });
                  },
                  child: stars[1],
                ),

                const SizedBox(width: 05),
                GestureDetector(
                  onTap: () {
                    changeRate(context, 3);
                    setState(() {
                      stars[0] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[1] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[2] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      // También puedes actualizar las estrellas anteriores
                      for (int i = 3; i < stars.length; i++) {
                        stars[i] = const Icon(Icons.star_border,
                            size: 30, color: Colors.white);
                      }
                    });
                  },
                  child: stars[2],
                ),

                const SizedBox(width: 05),
                GestureDetector(
                  onTap: () {
                    changeRate(context, 4);
                    setState(() {
                      stars[0] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[1] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[2] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[3] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      // También puedes actualizar las estrellas anteriores
                      for (int i = 4; i < stars.length; i++) {
                        stars[i] = const Icon(Icons.star_border,
                            size: 30, color: Colors.white);
                      }
                    });
                  },
                  child: stars[3],
                ),

                const SizedBox(width: 05),
                GestureDetector(
                  onTap: () {
                    changeRate(context, 5);
                    setState(() {
                      stars[0] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[1] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[2] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[3] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      stars[4] = const Icon(Icons.star,
                          size: 30, color: Colors.yellow);
                      // También puedes actualizar las estrellas anteriores
                      //for (int i = 5; i < stars.length; i++) {
                      //  stars[i] = const Icon(Icons.star, size: 30, color: Colors.yellow);
                      //}
                    });
                  },
                  child: stars[4],
                ),
                // ... Los otros GestureDetectors para las estrellas restantes
              ],
            ),
          ],
        ));
  }
}
