import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:teamup/database/teamup_hive.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/places_provider.dart';

class PlacesScreen extends StatefulWidget {
  @override
  const PlacesScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const PlacesScreen();

  @override
  // ignore: library_private_types_in_public_api
  _PlacesScreen createState() => _PlacesScreen();
}

class _PlacesScreen extends State<PlacesScreen> {
  bool locationEnabled = false;
  late GoogleMapController googleMapController;
  //Se instancia la db
  TeamUpHive storage = TeamUpHive();

  //marcador donde se pondrá el lugar
  static const Marker _kgoogleMeeting = Marker(
    markerId: MarkerId("current_marker"),
    infoWindow: InfoWindow(title: "Meeting location"),
    position: LatLng(4.60296, -74.06521),
    draggable: true,
  );
  //Lista de punteros para el mapa
  Set<Marker> markersList = {_kgoogleMeeting};

  static const CameraPosition initialCameraPosition =
      CameraPosition(target: LatLng(4.60296, -74.06521), zoom: 19);
//Controlador del mapa de google
  late double latitude;
  late double longitude;
  @override
  void dispose() {
    super.dispose();
  }

//Lo que selecciona el bottomAppBar
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Places"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            padding: const EdgeInsets.only(),
            alignment: Alignment.centerLeft,
          ),
          placesList(),
        ]),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: const Text('Clear Places list'),
        icon: const Icon(Icons.delete),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        onPressed: () {
          //Se elimina de hive los lugares
          storage.deletePlaces();
          //Se actualiza la pagina
          setState(() {});
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),
            label: 'Add label',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star_border_outlined),
            label: 'Community places',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.share),
            label: 'Share Places',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromRGBO(229, 36, 55, 1),
        onTap: _onItemTapped,
      ),
    );
  }

  Widget placesList() {
    List<String> places = storage.getPlaces();
    if (places.isNotEmpty == true) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: places.length,
          itemBuilder: (context, index) {
            final item = places[index];
            int matchId = index + 1;
            return ListTile(
                title: Text("Place #$matchId"),
                subtitle: Text(item),
                onTap: () {
                  _showMap(context, item, matchId);
                });
          });
    } else {
      return (const Text('No places go to maps'));
    }
  }

  // metodo que maneja las acciones de la navbar
  Future<void> _onItemTapped(int index) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        _selectedIndex = index;

        if (_selectedIndex == 0) [];
        if (_selectedIndex == 1) {
          Navigator.pushNamed(context, Routes.communityPlaces);
        }
        if (_selectedIndex == 2) {
          PlacesProvider().addPlaces();
          _showDialogCommunity(context);
        }
      });
    } else {
      // ignore: use_build_context_synchronously
      _showInternetDialog(context);
      // Not connected to any network.
    }
  }

  Widget map(String location) {
    List<String> splitedLoc = location.split(',');
    latitude = double.parse(splitedLoc[0]);
    longitude = double.parse(splitedLoc[1]);

    markersList.clear();
    markersList.add(Marker(
        markerId: const MarkerId('Place'),
        infoWindow: const InfoWindow(
          title: "Favorite Location",
        ),
        position: LatLng(latitude, longitude),
        draggable: false));

    return Scaffold(
        appBar: AppBar(
          title: const Text("Favorite Place"),
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        ),
        body: GoogleMap(
          initialCameraPosition: initialCameraPosition,
          markers: markersList,
          mapType: MapType.normal,
          myLocationEnabled: locationEnabled,
          onMapCreated: (GoogleMapController controller) {
            googleMapController = controller;
            googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(target: LatLng(latitude, longitude), zoom: 17)));
          },
          onCameraMove: (CameraPosition cameraPosition) {
            latitude = cameraPosition.target.latitude;
            longitude = cameraPosition.target.longitude;
          },
        ));
  }

  _showMap(BuildContext context, String location, index) {
    showDialog(
      builder: (context) => map(location),
      context: context,
    );
  }

  //Internet dialog
  _showInternetDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("No internet connection found!"),
            Icon(
              Icons.warning,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("Please enable your internet and try again."),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

  _showDialogCommunity(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Thank you!"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("We shared your favorite Places in the community!"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.tracking);
            },
            child: const Text("Go to Tracking!"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }
}
