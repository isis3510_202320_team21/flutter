import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/matches.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/matches_provider.dart';
import 'package:connectivity/connectivity.dart';

class CommunityMatchesScreen extends StatefulWidget {
  const CommunityMatchesScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: MatchesProvider())],
        child: const CommunityMatchesScreen());
  }

  @override
  State<StatefulWidget> createState() => _CommunityMatchesScreenState();
}

class _CommunityMatchesScreenState extends State<CommunityMatchesScreen> {
  final teamSearchController = TextEditingController();
  List<MyMatches> listaFiltrada = List.empty();

  asignarLista(BuildContext context) async {
    listaFiltrada = await MatchesProvider().getMatches();
    return listaFiltrada;
  }

//Metodo que verifica la conectividad
  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(children: [
            const SizedBox(
              height: 100,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Community Matches!',
                textAlign: TextAlign.center,
                textScaleFactor: 3,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.black),
              )),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              height: 16,
            ),

            buildMatchesList(context),

            const SizedBox(
              height: 16,
            ),
            //Aqui deberia haber padding
          ])),
        ));
  }

  Widget buildMatchesList(BuildContext context) {
    return FutureBuilder(
      future: asignarLista(context),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    height: 400,
                    width: double.infinity,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: listaFiltrada.length,
                        itemBuilder: (context, index) {
                          final match = listaFiltrada[index];
                          String date = match.date;
                          String team = match.team;
                          String result = match.result;
                          String place = match.place;
                          String dateParsed = date.split(':')[0];
                          String reOrder = dateParsed.split(' ')[0];
                          String reOrder2 = dateParsed.split(' ')[1];
                          if (result.startsWith("W") == true) {
                            return ListTile(
                                title: Text(
                                  "Match with $team at $reOrder2 $reOrder",
                                  style: const TextStyle(
                                      fontFamily: 'BungeeInLine'),
                                ),
                                subtitle: Center(
                                    child: Text(
                                  result,
                                  style: const TextStyle(
                                      fontFamily: 'BungeeInLine',
                                      color: Colors.green),
                                )),
                                onTap: () {
                                  _showDialog(
                                      context, result, place, team, date);
                                });
                          } else {
                            return ListTile(
                                title: Text(
                                  "Match with $team at $reOrder2 $reOrder",
                                  style: const TextStyle(
                                      fontFamily: 'BungeeInLine'),
                                ),
                                subtitle: Center(
                                    child: Text(
                                  result,
                                  style: const TextStyle(
                                      fontFamily: 'BungeeInLine',
                                      color: Colors.red),
                                )),
                                onTap: () {
                                  _showDialog(
                                      context, result, place, team, date);
                                });
                          }
                        }))
              ],
            );
          } else {
            return genericFallback();
          }
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return const Center(
            child: Text("No internet connection"),
          );
        }
      },
    );
  }

  _showDialog(BuildContext context, String result, String place, String name,
      String date) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Match Details"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: Text(
            "You played with team $name and $result at $date in place: $place"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.tracking);
            },
            child: const Text("Go to Tracking"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Image of generic fallback
  Widget mascot() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'lib/img/muñeco.png',
        height: 150,
        width: 150,
      ),
    );
  }

  //Generic fallback screen
  Widget genericFallback() {
    return Column(
      children: [
        const SizedBox(height: 10),
        const SizedBox(height: 10),
        mascot(),
        const SizedBox(height: 10),
        Align(
            alignment: Alignment.bottomCenter,
            child: Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                color: const Color.fromRGBO(229, 36, 55, 1),
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
                    child: const Center(
                        child: Text(
                            "Oops, seems like you dont have internet! restart the page.",
                            style: TextStyle(
                                fontSize: 20, color: Colors.white)))))),
      ],
    );
  }
}
