import 'package:blurry/blurry.dart';
import 'package:connectivity/connectivity.dart';
import 'package:easy_actions/easy_actions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/routine.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/routines_provider.dart';
import 'package:teamup/screen/routines_list_screen.dart';
import 'package:teamup/screen/pdf_screen.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class RoutinesScreen extends StatefulWidget {
  const RoutinesScreen({Key? key}) : super(key: key);
  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: RoutinesProvider())],
        child: const RoutinesScreen());
  }

  @override
  State<RoutinesScreen> createState() => _RoutinesScreenState();
}

class _RoutinesScreenState extends State<RoutinesScreen> {
  final keyController = TextEditingController();
  String keyValue = "";
  Set<String> selectedCategories = <String>{};
  Set<String> selectedLanguages = <String>{};
  Color color1 = Colors.white;
  Color color2 = Colors.white;
  Color color3 = Colors.white;
  Color color4 = Colors.white;
  Color color5 = Colors.white;
  Color color6 = Colors.white;
  Color color7 = Colors.white;
  Color color8 = Colors.white;
  Color color9 = Colors.white;
  Color color10 = Colors.white;
  Color color11 = Colors.white;
  Color color12 = Colors.white;
  Color color13 = Colors.white;
  Color color14 = Colors.white;
  Color color15 = Colors.white;
  Color color16 = Colors.white;
  Color color17 = Colors.white;
  Color color18 = Colors.white;
  Color color19 = Colors.white;
  Color color20 = Colors.white;
  Color color21 = Colors.white;
  Color color22 = Colors.white;
  Color color23 = Colors.white;
  Color color24 = Colors.white;
  Color color25 = Colors.white;
  Color color26 = Colors.white;
  Color color27 = Colors.white;
  Color color28 = Colors.white;
  Color color29 = Colors.white;

  void handleCategoryPressed(String buttonName) {
    if (selectedCategories.contains(buttonName)) {
      selectedCategories.remove(buttonName);
    } else {
      selectedCategories.add(buttonName);
    }
    // Imprime el conjunto actualizado
  }

  void handleLanguagesPressed(String buttonName) {
    if (selectedLanguages.contains(buttonName)) {
      selectedLanguages.remove(buttonName);
    } else {
      selectedLanguages.add(buttonName);
    }
    // Imprime el conjunto actualizado
  }

  Future<List<MyRoutine>> getExercisesFiltred(
      String texto, BuildContext context) {
    //Conviertes el texto en lista(?)
    return Provider.of<RoutinesProvider>(context, listen: false)
        .fetchExercisesByParameter(
            texto, selectedCategories, selectedLanguages);
  }

  Future<List<MyRoutine>> getExercises(BuildContext context) {
    //Conviertes el texto en lista(?)
    return Provider.of<RoutinesProvider>(context, listen: false)
        .fetchExercisesComplete(selectedCategories, selectedLanguages);
  }

  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  Widget buildSearchPage(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(24),
        child: Center(
          child: ListView(children: [
            const SizedBox(
              height: 110,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Search routines',
                textAlign: TextAlign.center,
                textScaleFactor: 3,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            TextFormField(
              controller: keyController,
              maxLength: 30,
              decoration: const InputDecoration(
                  labelText: 'key words',
                  filled: true,
                  fillColor: Colors.white,
                  icon: Icon(Icons.spellcheck_rounded, color: Colors.white)),
              style: const TextStyle(color: Colors.black),
              onChanged: (value) {
                setState(() {
                  keyValue = value;
                });
              },
            ),
            const SizedBox(
              height: 8,
            ),
            const SizedBox(
              height: 50,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Categories',
                textAlign: TextAlign.center,
                textScaleFactor: 2.5,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                EasyElevatedButton(
                  label: "Abs",
                  isRounded: true,
                  color: color2,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color2 == Colors.red) {
                        color2 = Colors.white;
                        handleCategoryPressed("Abs");
                      } else {
                        color2 = Colors.red;
                        handleCategoryPressed("Abs");
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Arms",
                  isRounded: true,
                  color: color3,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color3 == Colors.red) {
                        color3 = Colors.white;
                        handleCategoryPressed("Arms");
                      } else {
                        color3 = Colors.red;
                        handleCategoryPressed("Arms");
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Back",
                  isRounded: true,
                  color: color4,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color4 == Colors.red) {
                        color4 = Colors.white;
                        handleCategoryPressed("Back");
                      } else {
                        color4 = Colors.red;
                        handleCategoryPressed("Back");
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Calves",
                  isRounded: true,
                  color: color5,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color5 == Colors.red) {
                        color5 = Colors.white;
                        handleCategoryPressed("Calves");
                      } else {
                        color5 = Colors.red;
                        handleCategoryPressed("Calves");
                      }
                    });
                  },
                ),
              ],
            ),
            Wrap(
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              children: [
                EasyElevatedButton(
                  label: "Cardio",
                  isRounded: true,
                  color: color6,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color6 == Colors.red) {
                        color6 = Colors.white;
                        handleCategoryPressed("Cardio");

                      } else {
                        color6 = Colors.red;
                        handleCategoryPressed("Cardio");
                        
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Chest",
                  isRounded: true,
                  color: color7,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color7 == Colors.red) {
                        color7 = Colors.white;
                        handleCategoryPressed("Chest");
                      } else {
                        color7 = Colors.red;
                        handleCategoryPressed("Chest");
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Legs",
                  isRounded: true,
                  color: color8,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color8 == Colors.red) {
                        color8 = Colors.white;
                        handleCategoryPressed("Legs");
                      } else {
                        color8 = Colors.red;
                        handleCategoryPressed("Legs");
                      }
                    });
                  },
                ),
                const SizedBox(
                  width: 8,
                ),
                EasyElevatedButton(
                  label: "Shoulders",
                  isRounded: true,
                  color: color9,
                  labelColor: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (color9 == Colors.red) {
                        color9 = Colors.white;
                        handleCategoryPressed("Shoulders");
                      } else {
                        color9 = Colors.red;
                        handleCategoryPressed("Shoulders");
                      }
                    });
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 50,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Languages',
                textAlign: TextAlign.center,
                textScaleFactor: 2.5,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            SizedBox(
              height: 200,
              child: ListView(children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Azərbaycan dili",
                      isRounded: true,
                      color: color10,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color10 == Colors.red) {
                            color10 = Colors.white;
                            handleLanguagesPressed("Azərbaycan dili");
                          } else {
                            color10 = Colors.red;
                            handleLanguagesPressed("Azərbaycan dili");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Bahasa Indonesia",
                      isRounded: true,
                      color: color11,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color11 == Colors.red) {
                            color11 = Colors.white;
                            handleLanguagesPressed("Bahasa Indonesia");
                          } else {
                            color11 = Colors.red;
                            handleLanguagesPressed("Bahasa Indonesia");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Čeština",
                      isRounded: true,
                      color: color12,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color12 == Colors.red) {
                            color12 = Colors.white;
                            handleLanguagesPressed("Čeština");
                          } else {
                            color12 = Colors.red;
                            handleLanguagesPressed("Čeština");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Deutsch",
                      isRounded: true,
                      color: color13,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color13 == Colors.red) {
                            color13 = Colors.white;
                            handleLanguagesPressed("Deutsch");
                          } else {
                            color13 = Colors.red;
                            handleLanguagesPressed("Deutsch");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "English",
                      isRounded: true,
                      color: color14,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color14 == Colors.red) {
                            color14 = Colors.white;
                            handleLanguagesPressed("English");
                          } else {
                            color14 = Colors.red;
                            handleLanguagesPressed("English");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Español",
                      isRounded: true,
                      color: color15,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color15 == Colors.red) {
                            color15 = Colors.white;
                            handleLanguagesPressed("Español");
                          } else {
                            color15 = Colors.red;
                            handleLanguagesPressed("Español");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Esperanto",
                      isRounded: true,
                      color: color16,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color16 == Colors.red) {
                            color16 = Colors.white;
                            handleLanguagesPressed("Esperanto");
                          } else {
                            color16 = Colors.red;
                            handleLanguagesPressed("Esperanto");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Français",
                      isRounded: true,
                      color: color17,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color17 == Colors.red) {
                            color17 = Colors.white;
                            handleLanguagesPressed("Français");
                          } else {
                            color17 = Colors.red;
                            handleLanguagesPressed("Français");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Hrvatski jezik",
                      isRounded: true,
                      color: color18,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color18 == Colors.red) {
                            color18 = Colors.white;
                            handleLanguagesPressed("Hrvatski jezik");
                          } else {
                            color18 = Colors.red;
                            handleLanguagesPressed("Hrvatski jezik");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Italian",
                      isRounded: true,
                      color: color19,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color19 == Colors.red) {
                            color19 = Colors.white;
                            handleLanguagesPressed("Italian");
                          } else {
                            color19 = Colors.red;
                            handleLanguagesPressed("Italian");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Nederlands",
                      isRounded: true,
                      color: color20,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color20 == Colors.red) {
                            color20 = Colors.white;
                            handleLanguagesPressed("Nederlands");
                          } else {
                            color20 = Colors.red;
                            handleLanguagesPressed("Nederlands");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Norsk",
                      isRounded: true,
                      color: color21,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color21 == Colors.red) {
                            color21 = Colors.white;
                            handleLanguagesPressed("Norsk");
                          } else {
                            color21 = Colors.red;
                            handleLanguagesPressed("Norsk");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Polish",
                      isRounded: true,
                      color: color22,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color22 == Colors.red) {
                            color22 = Colors.white;
                            handleLanguagesPressed("Polish");
                          } else {
                            color22 = Colors.red;
                            handleLanguagesPressed("Polish");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Português",
                      isRounded: true,
                      color: color23,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color23 == Colors.red) {
                            color23 = Colors.white;
                            handleLanguagesPressed("Português");
                          } else {
                            color23 = Colors.red;
                            handleLanguagesPressed("Português");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Svenska",
                      isRounded: true,
                      color: color24,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color24 == Colors.red) {
                            color24 = Colors.white;
                            handleLanguagesPressed("Svenska");
                          } else {
                            color24 = Colors.red;
                            handleLanguagesPressed("Svenska");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Türkçe",
                      isRounded: true,
                      color: color25,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color25 == Colors.red) {
                            color25 = Colors.white;
                            handleLanguagesPressed("Türkçe");
                          } else {
                            color25 = Colors.red;
                            handleLanguagesPressed("Türkçe");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Ελληνικά",
                      isRounded: true,
                      color: color26,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color26 == Colors.red) {
                            color26 = Colors.white;
                            handleLanguagesPressed("Ελληνικά");
                          } else {
                            color26 = Colors.red;
                            handleLanguagesPressed("Ελληνικά");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Български език",
                      isRounded: true,
                      color: color27,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color27 == Colors.red) {
                            color27 = Colors.white;
                            handleLanguagesPressed("Български език");
                          } else {
                            color27 = Colors.red;
                            handleLanguagesPressed("Български език");
                          }
                        });
                      },
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    EasyElevatedButton(
                      label: "Русский",
                      isRounded: true,
                      color: color28,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color28 == Colors.red) {
                            color28 = Colors.white;
                            handleLanguagesPressed("Русский");
                          } else {
                            color28 = Colors.red;
                            handleLanguagesPressed("Русский");
                          }
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    EasyElevatedButton(
                      label: "Українська мова",
                      isRounded: true,
                      color: color29,
                      labelColor: Colors.black,
                      onPressed: () {
                        setState(() {
                          if (color29 == Colors.red) {
                            color29 = Colors.white;
                            handleLanguagesPressed("Українська мова");
                          } else {
                            color29 = Colors.red;
                            handleLanguagesPressed("Українська мова");
                          }
                        });
                      },
                    ),
                  ],
                ),
              ]),
            ),
            Wrap(
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                    ),
                    onPressed: () async {
                      Stream<bool> xd = Stream.fromFuture(messageNet(context));
                      // ignore: unrelated_type_equality_checks
                      if (xd == true) {
                        if (selectedCategories.isEmpty) {
                          // ignore: use_build_context_synchronously
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Please select a category'),
                          ));
                        } else if (selectedLanguages.isEmpty) {
                          // ignore: use_build_context_synchronously
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Please select a language'),
                          ));
                        } else {
                          // ignore: use_build_context_synchronously
                          getExercises(context).then((value) {
                            if (value.isEmpty) {
                              Blurry.info(
                                title: 'Ups!',
                                description:
                                    "Unfortunately, we do not have routines of these characteristics yet. Try again with other features",
                                confirmButtonText: 'Try again',
                                onConfirmButtonPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ).show(context);
                            } else {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        ListRoutineScreen(value),
                                  ));
                            }
                          });
                        }
                      } else {
                        // ignore: use_build_context_synchronously
                        Blurry.info(
                          title: 'Ups!',
                          description:
                              "It seems your device soesn't have internet connection, this feature requires connection, please check and try again",
                          confirmButtonText: 'Try again',
                          onConfirmButtonPressed: () {
                            Navigator.of(context).pop();
                          },
                        ).show(context);
                      }
                    },
                    child: const Text("Search without key word")),
                const SizedBox(
                  width: 8,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                    ),
                    onPressed: () async {
                      Stream<bool> xd = Stream.fromFuture(messageNet(context));
                      // ignore: unrelated_type_equality_checks
                      if (xd == true) {
                        if (selectedCategories.isEmpty) {
                          // ignore: use_build_context_synchronously
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Please select a category'),
                          ));
                        } else if (selectedLanguages.isEmpty) {
                          // ignore: use_build_context_synchronously
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Please select a language'),
                          ));
                        } else if (keyValue == "") {
                          // ignore: use_build_context_synchronously
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text(
                                'Please write a key word (hand, leg, warmup, etc)'),
                          ));
                        } else {
                          // ignore: use_build_context_synchronously
                          getExercisesFiltred(keyValue, context).then((value) {
                            if (value.isEmpty) {
                              Blurry.info(
                                title: 'Ups!',
                                description:
                                    "Unfortunately, we do not have routines of these characteristics yet. Try again with other features and key words",
                                confirmButtonText: 'Try again',
                                onConfirmButtonPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ).show(context);
                            } else {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        ListRoutineScreen(value),
                                  ));
                            }
                          });
                        }
                      } else {
                        // ignore: use_build_context_synchronously
                        Blurry.info(
                          title: 'Ups!',
                          description:
                              "It seems your device soesn't have internet connection, this feature requires connection, please check and try again",
                          confirmButtonText: 'Try again',
                          onConfirmButtonPressed: () {
                            Navigator.of(context).pop();
                          },
                        ).show(context);
                      }
                    },
                    child: const Text("Search with key")),
              ],
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
                onPressed: () =>
                    Navigator.pushNamed(context, Routes.sharedRoutines),
                child: const Text("Shared routines")),
            ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                        ),                    
                        onPressed: () async {
                          var fetchedFile = await DefaultCacheManager().getSingleFile("https://www.rockandwallclimbing.com/wp-content/uploads/2020/03/RUTINAS-DE-ENTRENAMIENTOS-EN-CASA.pdf");
                          // ignore: avoid_print
                          print(fetchedFile.path);    
                          // ignore: use_build_context_synchronously
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PDFScreen(path: fetchedFile.path),
                            ),
                          );
                        }, 
                        child: const Text("See one example of one routine"),   
                      ),
          ]),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: buildSearchPage(context),
    );
  }
}
