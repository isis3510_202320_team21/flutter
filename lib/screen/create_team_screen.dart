import 'package:blurry/blurry.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/team.dart';
import 'package:teamup/navigation/routes.dart';

import '../provider/teams_provider.dart';

class CreateTeamScreen extends StatefulWidget {
  const CreateTeamScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: TeamsProvider())],
        child: const CreateTeamScreen());
  }

  @override
  State<StatefulWidget> createState() => _CreateTeamScreenState();
}

class _CreateTeamScreenState extends State<CreateTeamScreen> {
  FlutterTts flutterTts = FlutterTts();
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final amountController = TextEditingController();
  String nameValue = "";
  String amountValue = "";
  Timestamp fecha = Timestamp.now();
  Timestamp base = Timestamp.now();
  final List<String> _options = [
    "noob",
    "amateur",
    "advanced",
    "expert",
    "pro"
  ];
  List<String> optionsSports = [
    "soccer",
    "micro-soccer",
    "beach volleyball",
    "volleyball",
    "Basketball",
    "ping pong"
  ];
  String sportSelected = "soccer";
  String selectedOption = "amateur";

  GoogleMapController? mapController; //contrller for Google map
  PolylinePoints polylinePoints = PolylinePoints();

  String googleAPiKey = "AIzaSyBcaGpXra3aoAiPhtBxQJ2Zpxhhb9UHSeA";

  Set<Marker> markers = {}; //markers for google map
  Map<PolylineId, Polyline> polylines = {}; //polylines to show direction

  LatLng startLocation = const LatLng(4.60296, -74.06521);
  LatLng endLocation = const LatLng(4.60296, -74.06521);

  double distance = 5.0;

  String text = "";

  void initSpeak() async {
    await flutterTts.setVolume(1);
    await flutterTts.setPitch(1);
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setLanguage("en-US");
  }

  void _speak(String text) async {
    await flutterTts.speak(text);
  }

  Future<LatLng?> _determinePosition() async {
    LocationData? currentLocation;
    var location = Location();
    try {
      // Find and store your location in a variable
      currentLocation = await location.getLocation();
      double latitude = currentLocation.latitude!;
      double longitude = currentLocation.longitude!;
      // Se mueve la camara a la posicion encontrada con el controlador
      LatLng temp = LatLng(latitude, longitude);
      return temp;
    } on Exception {
      currentLocation = null;
    }
    return null;
  }

  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  void buildPopUp(BuildContext context, String text) {
    Blurry.info(
      title: 'Creating team',
      description: text,
      confirmButtonText: 'Create',
      onConfirmButtonPressed: () {
        Provider.of<TeamsProvider>(context, listen: false)
            .addAndSaveTeam(MyTeam(
                date: fecha,
                level: selectedOption,
                members: amountValue,
                name: nameValue,
                sport: sportSelected))
            .then((_) {
          Navigator.pushNamed(context, Routes.teams);
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Team created!'),
            ),
          );
        });
      },
    ).show(context);
  }

  void seleccionarFecha() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2100));

    if (date != null) {
      final TimeOfDay? hora =
          // ignore: use_build_context_synchronously
          await showTimePicker(context: context, initialTime: TimeOfDay.now());

      if (hora != null) {
        final fechaFinal =
            DateTime(date.year, date.month, date.day, hora.hour, hora.minute);

        setState(() {
          fecha = Timestamp.fromDate(fechaFinal);
          nameValue = nameController.text;
          amountValue = amountController.text;
        });
      }
    }
  }

  Future getDistance(BuildContext context) {
    return Provider.of<TeamsProvider>(context, listen: false).calculateDistance(
        startLocation.latitude,
        startLocation.longitude,
        endLocation.latitude,
        endLocation.longitude);
  }

  Future getText(BuildContext context, double distance) {
    return Provider.of<TeamsProvider>(context, listen: false).getText(distance);
  }

  String? nameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "This is a required field";
    }
    return null;
  }

  String? amountValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "This is a required field";
    } else {
      int? value2 = int.tryParse(value);
      if (value2 == null) {
        return "This field must be a numeric value";
      } else {
        if (value2 > 22) {
          return "The member limit is 22 for the available sports";
        } else if (value2 < 2) {
          return "You can't create a team with 0 or 1 members";
        }
      }
    }
    return null;
  }

  String? dropTableValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Please select an option";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 0, 0),
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        actions: [
          IconButton(
              onPressed: () => Navigator.pushNamed(context, Routes.settings),
              icon: const Icon(Icons.list),
              color: Colors.white),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Form(
            key: formKey,
            child: ListView(
              children: [
                const SizedBox(
                  height: 16,
                ),
                TextFormField(
                  validator: nameValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: nameController,
                  maxLength: 30,
                  decoration: const InputDecoration(
                      labelText: 'Teams name',
                      filled: true,
                      fillColor: Colors.white,
                      icon:
                          Icon(Icons.spellcheck_rounded, color: Colors.white)),
                  style: const TextStyle(color: Colors.black),
                  onChanged: (value) {
                    setState(() {
                      nameValue = value;
                    });
                  },
                ),
                const SizedBox(
                  height: 8,
                ),
                TextFormField(
                  validator: amountValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: amountController,
                  maxLength: 30,
                  decoration: const InputDecoration(
                      labelText: '# of members',
                      filled: true,
                      fillColor: Colors.white,
                      icon:
                          Icon(Icons.spellcheck_rounded, color: Colors.white)),
                  style: const TextStyle(color: Colors.black),
                  onChanged: (value) {
                    setState(() {
                      amountValue = value;
                    });
                  },
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(
                  height: 8,
                ),
                DropdownButtonFormField(
                    validator: dropTableValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        filled: true, fillColor: Colors.white),
                    hint: const Text("Select one sport"),
                    items: optionsSports.map((e) {
                      return DropdownMenuItem(
                        value: e,
                        child: Text(e),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      if (value != null) sportSelected = value;
                    }),
                const SizedBox(
                  height: 24,
                ),
                DropdownButtonFormField(
                    validator: dropTableValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        filled: true, fillColor: Colors.white),
                    hint: const Text("Select level"),
                    items: _options.map((e) {
                      return DropdownMenuItem(
                        value: e,
                        child: Text(e),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      if (value != null) selectedOption = value;
                    }),
                const SizedBox(
                  height: 16,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: seleccionarFecha,
                  child: const Text('      Select date & time here       '),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () async {
                    bool xd = await messageNet(context);

                    // ignore: unrelated_type_equality_checks
                    if (xd == true) {
                      if (fecha == base) {
                        // ignore: use_build_context_synchronously
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Please select a date in the future'),
                          ),
                        );
                      }
                      if (formKey.currentState?.validate() == true) {
                        Future<LatLng?> futuro = _determinePosition();
                        futuro.then((value) async => {
                              if (value != null)
                                {
                                  startLocation = value,
                                  distance = await getDistance(context),
                                  text = await getText(context, distance),
                                  initSpeak(),
                                  _speak(text),
                                  buildPopUp(context, text)
                                }
                            });
                      }
                    } else {
                      // ignore: use_build_context_synchronously
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('You are offline, try again later'),
                        ),
                      );
                    }
                  },
                  child: const Text('Create team'),
                ),
              ],
            )),
      ),
    );
  }
}
