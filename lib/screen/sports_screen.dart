import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:provider/provider.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/sports_provider.dart';
import 'package:teamup/screen/pdf_screen.dart';
import '../model/sport.dart';
import 'package:connectivity/connectivity.dart';


class SportScreen extends StatefulWidget {
  const SportScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: SportProvider())],
        child: const SportScreen());
  }

  @override
  State<StatefulWidget> createState() => _SportScreenState();
}

class _SportScreenState extends State<SportScreen> {

  List<MySport> lista = List.empty();
//Metodo que verifica la conectividad
  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  void asignarLista(BuildContext context) {
    Future<List<MySport>> listaProvider = Provider.of<SportProvider>(context).getSports();
    listaProvider.then((value) => lista = value);
  }

  

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(children: [
            const SizedBox(
              height: 75,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'See which sports you could play',
                textAlign: TextAlign.center,
                textScaleFactor: 2,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            const SizedBox(
              height: 16,
            ),
            CachedNetworkImage(
                            width: 100,
                            height: 100,
                            imageUrl:
                                'https://www.shutterstock.com/image-photo/leather-soccer-ball-isolated-on-260nw-723600067.jpg',
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
            const SizedBox(
              height: 16,
            ),

            buildSportList(context),

            const SizedBox(
              height: 16,
            ),
    
            //Aqui deberia haber padding
          ])),
        ));
  }

  Widget buildSportList(BuildContext context) {
    asignarLista(context);
    if (lista.isEmpty) {
      Widget t = Container();
      Stream<bool> mensaje = Stream.fromFuture(messageNet(context));
      // ignore: unrelated_type_equality_checks
      if (mensaje == false) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Check your internet connection'),
          backgroundColor: Color.fromRGBO(229, 36, 55, 1),
        ));
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Loading sports...",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            )
          ],
        );
      } else {
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Check your internet connection... ",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 40, // Establece el ancho del indicador
              height: 40, // Establece la altura del indicador
              child: CircularProgressIndicator(
                strokeWidth: 4, // Cambia el tamaño del indicador
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.blue), // Cambia el color del indicador
              ),
            )
          ],
        );
      }
      return t;
    } else {
      return SizedBox(
        height: 400,
        width: double.infinity,
        child: ListView.builder(
            itemCount: lista.length,
            itemBuilder: ((context, index) {
              final item = lista[index];

              Color color = const Color.fromRGBO(229, 36, 55, 1);

                return SizedBox(
                  height: 130,
                  width: double.infinity,
                  child: RepaintBoundary(
                    child: Center(
                        child: Column(children: [
                      const SizedBox(
                        height: 16,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: color,
                        ),                    
                        onPressed: () async {
                          var fetchedFile = await DefaultCacheManager().getSingleFile(item.rules);
                          // ignore: avoid_print
                          print(fetchedFile.path);    
                          // ignore: use_build_context_synchronously
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PDFScreen(path: fetchedFile.path),
                            ),
                          );
                        },
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: color,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text(
                                item.name,
                                textAlign: TextAlign.center,
                                textScaleFactor: 2,
                                style: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                ),
                              ),
                            ),
                            Text("${item.players} players",
                                textAlign: TextAlign.center,
                                textScaleFactor: 1,
                                style: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                )),
                              const Text("Tap to download the rules",
                                textAlign: TextAlign.center,
                                textScaleFactor: 1,
                                style: TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                )),
                            
                          ],
                        )
                          
                      ),
                    ])),
                  ),
                );
              
            })),
      );
    }
  }
}
