import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:location/location.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import 'package:teamup/database/teamup_hive.dart';
import '../navigation/routes.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);
  static Widget create(BuildContext context) => const MapScreen();
  @override
  State<MapScreen> createState() => _MapScreenState();
}

const kGoogleApiKey = 'AIzaSyBcaGpXra3aoAiPhtBxQJ2Zpxhhb9UHSeA';
final homeScaffoldKey = GlobalKey<ScaffoldState>();

class _MapScreenState extends State<MapScreen> {
//Declaracion de variables
  // ignore: non_constant_identifier_names
  bool location_enabled = false;
  late double latitude;
  late double longitude;
  late Box mapBox;
  late GoogleMapController googleMapController;
  //Declaracion de la posicion inicial de la camara
  static const CameraPosition initialCameraPosition =
      CameraPosition(target: LatLng(4.60296, -74.06521), zoom: 19);

//Instancia de hive
  TeamUpHive storage = TeamUpHive();
  //Inicializacion del mapa
  void initialize() {
    latitude = 4.60296;
    longitude = -74.06521;
  }

  @override
  void dispose() {
    super.dispose();
  }

//marcador donde se pondrá el lugar
  static const Marker _kgoogleMeeting = Marker(
    markerId: MarkerId("current_marker"),
    infoWindow: InfoWindow(title: "Meeting location"),
    position: LatLng(4.60296, -74.06521),
    draggable: true,
  );
  //Lista de punteros para el mapa
  Set<Marker> markersList = {_kgoogleMeeting};

  static const CameraPosition _kCentered = CameraPosition(
    target: LatLng(4.60296, -74.06521),
    zoom: 17,
  );
  int _selectedIndex = 0;
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  @override
  Widget build(BuildContext context) {
    generarEvent();
    return Scaffold(
      key: homeScaffoldKey,
      appBar: AppBar(
        title: const Text("Team Up Where You Want!"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: GoogleMap(
        initialCameraPosition: initialCameraPosition,
        markers: markersList,
        mapType: MapType.normal,
        myLocationEnabled: location_enabled,
        onMapCreated: (GoogleMapController controller) {
          googleMapController = controller;
          initialize();
        },
        onCameraMove: (CameraPosition cameraPosition) {
          latitude = cameraPosition.target.latitude;
          longitude = cameraPosition.target.longitude;
        },
        // ignore: avoid_types_as_parameter_names, non_constant_identifier_names
        onTap: (LatLng) {
          //Se borra el marcador existente
          markersList.clear();
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: const Text('Select sports place'),
        icon: const Icon(Icons.navigation_outlined),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        onPressed: () {
          selectLocation();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.filter_center_focus_outlined),
            label: 'Center Pin',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_pin),
            label: 'My current location',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chrome_reader_mode_outlined),
            label: 'Go to Uniandes',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromRGBO(229, 36, 55, 1),
        onTap: _onItemTapped,
      ),
    );
  }

  // metodo que maneja las acciones de la navbar
  Future<void> _onItemTapped(int index) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        _selectedIndex = index;

        if (_selectedIndex == 0) [pinPlace()];
        if (_selectedIndex == 1) [_determinePosition()];
        if (_selectedIndex == 2) [goToUniandes()];
      });
    } else {
      // ignore: use_build_context_synchronously
      _showInternetDialog(context);
      // Not connected to any network.
    }
  }

  _showDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Sports location added to your favorites!"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: Text(
            "Meeting place with coordinates Lat: $latitude, Long: $longitude added to your favorite meeting places. You can now create further meetings in this location."),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.places);
            },
            child: const Text("Go to places"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Internet dialog
  _showInternetDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("No internet connection found!"),
            Icon(
              Icons.warning,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("Please enable your internet and try again."),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Duplicate Place dialog
  _showDuplicateDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Place created"),
            Icon(
              Icons.warning,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("You already added this location before!"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

  //Metodo para centrar el marcador en el mapa uniandes.
  Future<void> goToUniandes() async {
    //Por si no hay internet

    latitude = _kgoogleMeeting.position.latitude;
    longitude = _kgoogleMeeting.position.longitude;
    updateMarker();
    googleMapController
        .animateCamera(CameraUpdate.newCameraPosition(_kCentered));
  }

  //Metodo para encontrar la posicion actual y centrar el marcador
  Future<void> _determinePosition() async {
    LocationData? currentLocation;
    var location = Location();
    try {
      // Find and store your location in a variable
      currentLocation = await location.getLocation();
      latitude = currentLocation.latitude!;
      longitude = currentLocation.longitude!;
      // Se mueve la camara a la posicion encontrada con el controlador
      updateMarker();
    } on Exception {
      //Log in Crashlytics
      await FirebaseCrashlytics.instance
          .recordError('Location Error at map_screen', null);
      currentLocation = null;
    }
  }

//Metodo que almacena el lugar escogido por el usuario
  Future<void> selectLocation() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      //Se obtiene marcador actual
      Marker marcador = markersList.elementAt(0);
      //Obtiene la posicion actual del marcador
      latitude = marcador.position.latitude;
      longitude = marcador.position.longitude;

      //Almacenamiento de evento de agregar ubicacion
      await FirebaseAnalytics.instance
          .logEvent(name: 'fav_meeting', parameters: {
        'Location': '$latitude,$longitude',
        'hour': (DateFormat.yMMMEd().add_jm().format(DateTime.now()))
      });

      //Se crea la ubicacion
      String place = '$latitude,$longitude';
      if (storage.placeCreated(place) == false) {
        // Se abre la base de datos y se almacena
        storage.setPlace(place);
        // ignore: use_build_context_synchronously
        _showDialog(context);
      } else if (storage.placeCreated(place) == true) {
        // ignore: use_build_context_synchronously
        _showDuplicateDialog(context);
      }
    } else {
      // ignore: use_build_context_synchronously
      _showInternetDialog(context);
    }
  }

  Future<void> generarEvent() async {
    await analytics.logEvent(name: "externalService", parameters: {
      "serviceUsed": "GoogleMaps",
    });
  }

//Metodo que actualiza el marcador cuando hay un movimiento de camara
  void updateMarker() {
    markersList.clear();
    markersList.add(Marker(
        markerId: const MarkerId('Meeting Location'),
        infoWindow: const InfoWindow(
            title: "Meeting location",
            snippet:
                "Is this the place you want to meet? Drag and drop the marker for more precision"),
        position: LatLng(latitude, longitude),
        draggable: true));
    setState(() {}); //Sets new state
    googleMapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(latitude, longitude), zoom: 17)));
  }

  Future<void> pinPlace() async {
    markersList.clear();
    //marcador donde se pondrá el lugar
    double screenWidth = MediaQuery.of(context).size.width *
        MediaQuery.of(context).devicePixelRatio;
    double screenHeight = MediaQuery.of(context).size.height *
        MediaQuery.of(context).devicePixelRatio;

    double middleX = screenWidth / 2;
    double middleY = screenHeight / 2;

    ScreenCoordinate screenCoordinate =
        ScreenCoordinate(x: middleX.round(), y: middleY.round());
    LatLng middlePoint = await googleMapController.getLatLng(screenCoordinate);
    markersList.add(Marker(
        markerId: const MarkerId('Meeting Location'),
        infoWindow: const InfoWindow(
            title: "Meeting location",
            snippet: "Is this the place you want to meet?"),
        position: middlePoint,
        draggable: true));
    setState(() {});
    googleMapController
        .animateCamera(CameraUpdate.newLatLngZoom(middlePoint, 20.0));
  }

  await(Future<ConnectivityResult> checkConnectivity) {}
}
