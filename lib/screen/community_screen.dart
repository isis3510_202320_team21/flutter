import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/theme/strings.dart';

class CommunityScreen extends StatelessWidget {
  const CommunityScreen({super.key});

  @override
  build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
      body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: Column(
            children: [
              const SizedBox(
                width: double.infinity,
                child: RepaintBoundary(
                    child: Text(
                  'Welcome to the community!',
                  textAlign: TextAlign.center,
                  textScaleFactor: 3,
                  style: TextStyle(
                      fontFamily: 'BungeeInLine', color: Colors.black),
                )),
              ),
              const SizedBox(height: 20),
              karim(),
              const SizedBox(height: 20),
              Column(children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                        minimumSize: const Size.fromHeight(35)),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.communityMatches);
                    },
                    child: const Text("Community Matches")),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                      minimumSize: const Size.fromHeight(35),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.communityPlaces);
                    },
                    child: const Text("Community Places")),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                      minimumSize: const Size.fromHeight(35),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.sharedRoutines);
                    },
                    child: const Text("Community Routines")),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                      minimumSize: const Size.fromHeight(35),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.dailyVid);
                    },
                    child: const Text("Daily Video")),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                      minimumSize: const Size.fromHeight(35),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.motivatingVid);
                    },
                    child: const Text("Other Activities"))
              ]),
            ],
          ))),
    );
  }

//Image of the Screen view
  Widget karim() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: CachedNetworkImage(
            placeholder: (context, url) => const CircularProgressIndicator(),
            imageUrl:
                'https://img2.freepng.es/20180405/yjw/kisspng-karim-benzema-football-player-team-sport-real-madrid-5ac681e0e1ff95.8005561815229588169257.jpg',
            width: 150,
            height: 150));
  }
}
