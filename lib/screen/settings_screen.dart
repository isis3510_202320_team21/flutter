import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teamup/cubit/cubit_auth.dart';
import 'package:teamup/cubit/user_cubit.dart';
import 'package:teamup/model/user.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/repository/implementation/user_repository.dart';
import 'package:image_picker/image_picker.dart';

import '../cubit/state_auth.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => UserCubit(UserRepository())..getUser(),
      child: const SettingsScreen(),
    );
  }

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(229, 36, 55, 50),
        title: const Text('Welcome'),
        actions: [
          IconButton(
            onPressed: () => context.read<AuthCubit>().signOut(),
            icon: const Icon(Icons.logout),
          )
        ],
      ),
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserReadyState) {
            return _MyUserSection(
              user: state.user,
              pickedImage: state.pickedImage,
              isSaving: state.isSaving,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class _MyUserSection extends StatefulWidget {
  final MyUser? user;
  final File? pickedImage;
  final bool isSaving;

  const _MyUserSection({this.user, this.pickedImage, this.isSaving = false});

  @override
  State<_MyUserSection> createState() => __MyUserSectionState();
}

class __MyUserSectionState extends State<_MyUserSection> {
  final _idController = TextEditingController();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _ageController = TextEditingController();
  final _descriptionController = TextEditingController();

  final picker = ImagePicker();

  @override
  void initState() {
    _idController.text = widget.user?.id ?? '';
    _nameController.text = widget.user?.name ?? '';
    _emailController.text = widget.user?.email ?? '';
    _ageController.text = widget.user?.age.toString() ?? '';
    _descriptionController.text = widget.user?.description ?? '';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget image = Image.asset(
      'lib/img/muñeco.png',
      fit: BoxFit.fill,
    );

    if (widget.pickedImage != null) {
      image = Image.file(
        widget.pickedImage!,
        fit: BoxFit.fill,
      );
    } else if (widget.user?.image != null && widget.user!.image!.isNotEmpty) {
      image = CachedNetworkImage(
        imageUrl: widget.user!.image!,
        progressIndicatorBuilder: (_, __, progress) =>
            CircularProgressIndicator(value: progress.progress),
        errorWidget: (_, __, ___) => const Icon(Icons.error),
        fit: BoxFit.fill,
      );
    }

    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () async {
                final myUserCubit = context.read<UserCubit>();
                final pickedImage =
                    await picker.pickImage(source: ImageSource.gallery);
                if (pickedImage != null) {
                  myUserCubit.setImage(File(pickedImage.path));
                }
              },
              child: Center(
                child: ClipOval(
                  child: SizedBox(
                    width: 150,
                    height: 150,
                    child: image,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const SizedBox(
              height: 8,
            ),
            TextField(
              controller: _nameController,
              maxLength: 30,
              decoration: const InputDecoration(
                  labelText: 'Name',
                  filled: true,
                  fillColor: Color.fromRGBO(245, 184, 0, 1),
                  icon: Icon(
                    Icons.spellcheck_rounded,
                    color: Colors.white,
                  )),
              style: const TextStyle(color: Colors.black),
            ),
            TextField(
              controller: _emailController,
              decoration: const InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Color.fromRGBO(229, 36, 55, 50),
                  icon: Icon(
                    Icons.spellcheck_rounded,
                    color: Colors.white,
                  )),
              style: const TextStyle(color: Colors.white),
              enabled: false,
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: _ageController,
              decoration: const InputDecoration(
                  labelText: 'Age',
                  filled: true,
                  fillColor: Color.fromRGBO(245, 184, 0, 1),
                  icon: Icon(
                    Icons.spellcheck_rounded,
                    color: Colors.white,
                  )),
              style: const TextStyle(color: Colors.black),
              maxLength: 3,
              keyboardType: TextInputType.number,
            ),
            TextField(
              controller: _descriptionController,
              decoration: const InputDecoration(
                  labelText: 'Description',
                  labelStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Color.fromRGBO(229, 36, 55, 50),
                  icon: Icon(
                    Icons.spellcheck_rounded,
                    color: Colors.white,
                  )),
              style: const TextStyle(color: Colors.white),
              maxLength: 500,
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              width: 300,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                  textStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                onPressed: widget.isSaving
                    ? null
                    : () async {
                        await context.read<UserCubit>().saveUser(
                            (context.read<AuthCubit>().state as AuthSignedIn)
                                .user
                                .uid,
                            _nameController.text,
                            widget.user!.email,
                            int.tryParse(_ageController.text) ?? 0,
                            _descriptionController.text);
                        // ignore: use_build_context_synchronously
                        Navigator.pushNamed(context, Routes.home);

                        // ignore: use_build_context_synchronously
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('User Information Updated Correctly'),
                          ),
                        );
                      },
                child: const Text('Update Information'),
              ),
            ),
            if (widget.isSaving) const CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
