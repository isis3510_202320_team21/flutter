import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/tournament_provider.dart';
import '../model/tournament.dart';
import 'package:connectivity/connectivity.dart';

class TournamentScreen extends StatefulWidget {
  const TournamentScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: TournamentProvider())],
        child: const TournamentScreen());
  }

  @override
  State<StatefulWidget> createState() => _TournamentScreenState();
}

class _TournamentScreenState extends State<TournamentScreen> {
  List<MyTournament> listaFiltrada = List.empty();

  void asignarLista(BuildContext context) {
    Future<List<MyTournament>> lista = Provider.of<TournamentProvider>(context).getTournaments();
    lista.then((value) => listaFiltrada = value);
  }

//Metodo que verifica la conectividad
  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  @override
  Widget build(BuildContext context) {
    bool xd;
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(children: [
            const SizedBox(
              height: 75,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Active tournaments',
                textAlign: TextAlign.center,
                textScaleFactor: 2,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              height: 16,
            ),

            buildTournamentList(context),

            const SizedBox(
              height: 16,
            ),
            Container(
              height: 85,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(229, 36, 55, 1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
                  onPressed: () async => {
                        xd = await messageNet(context),
                        if (xd == true)
                          {Navigator.pushNamed(context, Routes.createTournament)}
                        else
                          {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('Offline'),
                              ),
                            )
                          }
                      },
                  child: const Text(
                    'CREATE TOURNAMENT',
                    textAlign: TextAlign.center,
                    textScaleFactor: 2,
                    style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Share Tech',
                    ),
                  )),
            )
            //Aqui deberia haber padding
          ])),
        ));
  }

  Widget buildTournamentList(BuildContext context) {
    asignarLista(context);
    if (listaFiltrada.isEmpty) {
      Widget t = Container();
      Stream<bool> mensaje = Stream.fromFuture(messageNet(context));
      // ignore: unrelated_type_equality_checks
      if (mensaje == false) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Check your internet connection'),
          backgroundColor: Color.fromRGBO(229, 36, 55, 1),
        ));
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Loading tournaments...",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            )
          ],
        );
      } else {
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Check your internet connection... ",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 40, // Establece el ancho del indicador
              height: 40, // Establece la altura del indicador
              child: CircularProgressIndicator(
                strokeWidth: 4, // Cambia el tamaño del indicador
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.blue), // Cambia el color del indicador
              ),
            )
          ],
        );
      }
      return t;
    } else {
      return SizedBox(
        height: 400,
        width: double.infinity,
        child: ListView.builder(
            itemCount: listaFiltrada.length,
            itemBuilder: ((context, index) {
              final item = listaFiltrada[index];

              Color color = const Color.fromRGBO(229, 36, 55, 1);
                return SizedBox(
                  height: 130,
                  width: double.infinity,
                  child: RepaintBoundary(
                    child: Center(
                        child: Column(children: [
                      const SizedBox(
                        height: 16,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: color,
                        ),
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 8,
                            ),
                            Container(

                              decoration: BoxDecoration(
                                color: color,
                                borderRadius: BorderRadius.circular(5),
                                
                              ),
                              child: Text(
                                item.name,
                                textAlign: TextAlign.center,
                                textScaleFactor: 2,
                                style: const TextStyle(
                                  color: Color.fromRGBO(244, 244, 244, 1),
                                  fontFamily: 'Share Tech',
                                ),
                              ),
                            ),
                                Text("${item.sport} tournament",
                                textAlign: TextAlign.center,
                                textScaleFactor: 2,
                                style: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                )),
                          ],
                        ),
                        onPressed: () => showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: Text(item.name),
                            content: Text(
                              'Remember your ${item.sport} tournament! \nThere are ${item.teams} teams in this tournament\n\nGood luck!!!'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text('Cancel'),
                              ),
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'OK'),
                                child: const Text('OK'),
                              ),
                            ],
                          ),
                        ),
                        )]
                    )
                    ),
                  ),
                );
              
            })),
      );
    }
  }
}
