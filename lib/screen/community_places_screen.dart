import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/places.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/places_provider.dart';
import 'package:connectivity/connectivity.dart';

class CommunityPlacesScreen extends StatefulWidget {
  const CommunityPlacesScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: PlacesProvider())],
        child: const CommunityPlacesScreen());
  }

  @override
  State<StatefulWidget> createState() => _CommunityPlacesScreenState();
}

class _CommunityPlacesScreenState extends State<CommunityPlacesScreen> {
  final teamSearchController = TextEditingController();
  List<MyPlaces> listaFiltrada = List.empty();

  //Variables necesarias para la creación del mapa
  //marcador donde se pondrá el lugar
  static const Marker _kgoogleMeeting = Marker(
    markerId: MarkerId("current_marker"),
    infoWindow: InfoWindow(title: "Meeting location"),
    position: LatLng(4.60296, -74.06521),
    draggable: true,
  );
  //Lista de punteros para el mapa
  Set<Marker> markersList = {_kgoogleMeeting};

  static const CameraPosition initialCameraPosition =
      CameraPosition(target: LatLng(4.60296, -74.06521), zoom: 19);
//Controlador del mapa de google
  late double latitude;
  late double longitude;
//Metodo que obtiene la lista de lugares
  asignarLista(BuildContext context) async {
    listaFiltrada = await PlacesProvider().getPlaces();
    return listaFiltrada;
  }

  bool locationEnabled = false;
  late GoogleMapController googleMapController;
//Metodo que verifica la conectividad
  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(children: [
            const SizedBox(
              height: 100,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Community Places!',
                textAlign: TextAlign.center,
                textScaleFactor: 3,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.black),
              )),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              height: 16,
            ),

            buildMatchesList(context),

            FloatingActionButton.extended(
              label: const Text('View in map'),
              icon: const Icon(Icons.navigation_outlined),
              backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
              onPressed: () {
                _showMap(context, listaFiltrada);
              },
            ),
            //Aqui deberia haber padding
          ])),
        ));
  }

  Widget buildMatchesList(BuildContext context) {
    return FutureBuilder(
      future: asignarLista(context),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    height: 400,
                    width: double.infinity,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: listaFiltrada.length,
                        itemBuilder: (context, index) {
                          final match = listaFiltrada[index];
                          int num = index + 1;
                          return ListTile(
                              title: Text(
                                "Place $num",
                                style:
                                    const TextStyle(fontFamily: 'BungeeInLine'),
                              ),
                              subtitle: Center(
                                  child: Text(
                                match.location,
                                style: const TextStyle(
                                    fontFamily: 'BungeeInLine',
                                    color: Colors.grey),
                              )),
                              onTap: () {});
                        })),
              ],
            );
          } else {
            return genericFallback();
          }
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return const Center(
            child: Text("No internet connection"),
          );
        }
      },
    );
  }

//Community Matches in a map
  Widget maps(List<MyPlaces> listaFiltrada) {
    //Se declaran la variables
    double latitude = 0.0;
    double longitude = 0.0;
    //Se saca el tamanio de la lista
    int size = listaFiltrada.length;
    //Se limpia la lista
    markersList.clear();
    for (int i = 0; i < size; i++) {
      //Se obtiene la latitud y longitud en formato
      List<String> splitedLoc = listaFiltrada[i].location.split(',');
      latitude = double.parse(splitedLoc[0]);
      longitude = double.parse(splitedLoc[1]);
      int num = i + 1;
      String id = 'place $num';
      markersList.add(Marker(
          markerId: MarkerId(id),
          infoWindow: InfoWindow(
            title: id,
          ),
          position: LatLng(latitude, longitude),
          draggable: false));
    }
    return Scaffold(
        appBar: AppBar(
          title: const Text("Community Places"),
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        ),
        body: GoogleMap(
          initialCameraPosition: initialCameraPosition,
          markers: markersList,
          mapType: MapType.normal,
          myLocationEnabled: locationEnabled,
          onMapCreated: (GoogleMapController controller) {
            googleMapController = controller;
            googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(target: LatLng(latitude, longitude), zoom: 17)));
          },
          onCameraMove: (CameraPosition cameraPosition) {
            latitude = cameraPosition.target.latitude;
            longitude = cameraPosition.target.longitude;
          },
        ));
  }

  _showMap(BuildContext context, List<MyPlaces> listaFiltrada) {
    showDialog(
      builder: (context) => maps(listaFiltrada),
      context: context,
    );
  }

  // ignore: unused_element
  _showDialog(BuildContext context, String result, String place, String name,
      String date) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Match Details"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: Text(
            "You played with team $name and $result at $date in place: $place"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.tracking);
            },
            child: const Text("Go to Tracking"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Image of the Screen view
  Widget mascot() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'lib/img/matches_image.jpg',
        height: 150,
        width: 150,
      ),
    );
  }

  //Generic fallback screen
  Widget genericFallback() {
    return Column(
      children: [
        const SizedBox(height: 10),
        const SizedBox(height: 10),
        mascot(),
        const SizedBox(height: 10),
        Align(
            alignment: Alignment.bottomCenter,
            child: Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                color: const Color.fromRGBO(229, 36, 55, 1),
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
                    child: const Center(
                        child: Text(
                            "Oops, seems like you dont have internet! restart the page.",
                            style: TextStyle(
                                fontSize: 20, color: Colors.white)))))),
      ],
    );
  }
}
