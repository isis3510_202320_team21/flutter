import 'package:flutter/material.dart';
import 'package:teamup/navigation/routes.dart';
import 'dart:core';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

import '../model/team.dart';

// ignore: camel_case_types
class team_detail_screen extends StatelessWidget {
  final MyTeam myTeam;

  const team_detail_screen(this.myTeam, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(
            children: [
              SizedBox(
                height: 150,
                width: double.infinity,
                child: RepaintBoundary(
                    child: Text(
                  '  ${myTeam.name}',
                  textAlign: TextAlign.center,
                  textScaleFactor: 3,
                  style: const TextStyle(
                      fontFamily: 'BungeeInLine', color: Colors.white),
                )),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                width: double.infinity,
                height: 75,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(229, 36, 55, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Text(
                  '# of members:  ${myTeam.members}',
                  textAlign: TextAlign.center,
                  textScaleFactor: 1.5,
                  style: const TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontFamily: 'Share Tech',
                  ),
                )),
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                width: double.infinity,
                height: 75,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(229, 36, 55, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Text(
                  'Level:  ${myTeam.level}',
                  textAlign: TextAlign.center,
                  textScaleFactor: 1.5,
                  style: const TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontFamily: 'Share Tech',
                  ),
                )),
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                width: double.infinity,
                height: 75,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(229, 36, 55, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Text(
                  'Sport:  ${myTeam.sport}',
                  textAlign: TextAlign.center,
                  textScaleFactor: 1.5,
                  style: const TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontFamily: 'Share Tech',
                  ),
                )),
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                width: double.infinity,
                height: 75,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(229, 36, 55, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Column(children: [
                  const Text(
                    'Date: ',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                    style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontFamily: 'Share Tech',
                    ),
                  ),
                  Text(
                    ' ${DateFormat('dd/MM/yyyy hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(myTeam.date.millisecondsSinceEpoch))}',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                    style: const TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontFamily: 'Share Tech',
                    ),
                  )
                ])),
              )
            ],
          )),
        ));
  }
}
