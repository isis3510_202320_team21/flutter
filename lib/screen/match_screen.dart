import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:teamup/database/shared_preferences.dart';

import '../model/team.dart';
import '../navigation/routes.dart';
import '../provider/teams_provider.dart';

class MatchScreen extends StatefulWidget {
  @override
  const MatchScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const MatchScreen();

  @override
  // ignore: library_private_types_in_public_api
  _MatchScreen createState() => _MatchScreen();
}

class _MatchScreen extends State<MatchScreen> {
  //Instanciacion de variables
  SharedPrefs prefs = SharedPrefs();
  Timestamp fecha = Timestamp.now();
  Timestamp base = Timestamp.now();
  //Se declaran arreglos de precargados de equipos
  // ignore: non_constant_identifier_names
  List<MyTeam> lista_filtrada = List.empty();
  // ignore: non_constant_identifier_names
  List<String> teamOptions = List.empty(growable: true);
  List<String> winOptions = ["Won", "Loss"];
  List<String> places = List.empty();
  String selectedPlace = "Uniandes";
  String selectedTeam = "Team";
  String selectedResult = "w";

  final formKey = GlobalKey<FormState>();
  //Lo que selecciona el bottomAppBar
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    //Se pide la instancia de las shared preferences
    prefs.sharePrefsInit();
    //Se obtienen los lugares
    places = getPlaces();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Track my matches"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: FutureBuilder(
        future: asignarLista(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //Create Weather widget
                  Center(child: createMatchForm()),
                ],
              );
            } else {
              return genericFallback();
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return const Center(
              child: Text("No internet connection"),
            );
          }
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.map_outlined),
            label: 'Create Places',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star_border_outlined),
            label: 'Community Matches',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people_outline),
            label: 'Create Teams',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromRGBO(229, 36, 55, 1),
        onTap: _onItemTapped,
      ),
    );
  }

  // metodo que maneja las acciones de la navbar
  Future<void> _onItemTapped(int index) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        _selectedIndex = index;

        if (_selectedIndex == 0) {
          //Se navega al mapa para crear lugar
          Navigator.pushNamed(context, Routes.map);
        }
        if (_selectedIndex == 1) [];
        if (_selectedIndex == 2) [Navigator.pushNamed(context, Routes.map)];
      });
    } else {
      // ignore: use_build_context_synchronously

      // Not connected to any network.
    }
  }

  Widget createMatchForm() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Form(
          key: formKey,
          child: ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: [
                mascot(),
                const SizedBox(
                  height: 16,
                ),
                dropTeamWidget(),
                const SizedBox(
                  height: 8,
                ),
                WinLossWidget(),
                const SizedBox(
                  height: 8,
                ),
                PlacesWidget(),
                const SizedBox(
                  height: 16,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: seleccionarFecha,
                  child: const Text('      Select date & time here       '),
                ),
                const SizedBox(
                  height: 16,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                      textStyle: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                    onPressed: () async {
                      if (selectedTeam == "Team") {
                      } else if (selectedResult == 'w') {
                      } else if (selectedPlace == "Uniandes") {
                      } else {
                        //Se crea el equipo
                        createMatch(
                            selectedTeam, selectedResult, selectedPlace, fecha);
                        //Se refresca la vista
                        setState(() {});
                        //Se muestra el dialogo de exito
                        _showDialog(context, selectedResult, selectedPlace,
                            selectedTeam);
                      }
                    },
                    child: const Text('      Create Match      ')),
              ])),
    );
  }

  //Metodo para obtener las partidas creadas
  getMatches() async {
    return await prefs.getMatches();
  }

  //Metodo para crear partidos
  void createMatch(
      String name, String result, String place, Timestamp date) async {
    //Se pone en formato
    DateTime dateF = DateTime.fromMillisecondsSinceEpoch(date.seconds * 1000);

    String dateStr =
        '${dateF.day}/${dateF.month}/${dateF.year} ${dateF.hour}:${dateF.minute}';
    String match = "$name, $result , $place, $dateStr";
    await prefs.setMatchInformation(match);
  }

//Elemento del formulario que muestra los equipos
  Widget dropTeamWidget() {
    return DropdownButtonFormField(
        isExpanded: true,
        validator: dropTableValidatorTeams,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration:
            const InputDecoration(filled: true, fillColor: Colors.white),
        hint: const Text("Select team"),
        items: teamOptions.map((e) {
          return DropdownMenuItem(
            value: e,
            child: Text(e),
          );
        }).toList(),
        onChanged: (String? value) {
          if (value != null) selectedTeam = value;
        });
  }

  //Elemento del formulario que muestra Win o Loss
  // ignore: non_constant_identifier_names
  Widget WinLossWidget() {
    return DropdownButtonFormField(
        validator: dropTableValidator,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration:
            const InputDecoration(filled: true, fillColor: Colors.white),
        hint: const Text("Select Result"),
        items: winOptions.map((e) {
          return DropdownMenuItem(
            value: e,
            child: Text(e),
          );
        }).toList(),
        onChanged: (String? value) {
          if (value != null) selectedResult = value;
        });
  }

  //Elemento del formulario que muestra Places
  // ignore: non_constant_identifier_names
  Widget PlacesWidget() {
    return DropdownButtonFormField(
        validator: dropTableValidatorPlaces,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration:
            const InputDecoration(filled: true, fillColor: Colors.white),
        hint: const Text("Select Place"),
        items: places.map((e) {
          return DropdownMenuItem(
            value: e,
            child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.6, child: Text(e)),
          );
        }).toList(),
        onChanged: (String? value) {
          if (value != null) selectedPlace = value;
        });
  }

//Metodo usado para cargar los distintos equipos a este formulario
  asignarLista() async {
    lista_filtrada = await TeamsProvider().getTeams();

    //Se asignan los nombres de equipos como opciones
    return await asignarNombresEquipos();
  }

//Metodo que extrae los nombres de los equipos en una lista
  asignarNombresEquipos() async {
    teamOptions = List.empty(growable: true);
    //Se crea iterador
    int i = 0;
    //Se obtiene el tamanio del arreglo
    int size = lista_filtrada.length;
    //Se hace el bucle
    while (i < size) {
      //Se saca el atributo deseado
      String nombre = lista_filtrada[i].name;
      //Se asigna el nombre al arreglo
      teamOptions.add(nombre);
      //Se sigue iterando
      i++;
    }
    return teamOptions;
  }

  List<String> getPlaces() {
    //Se abre la caja
    var mapBox = Hive.box("local_storage");
    //Se obtiene el tamaño de la caja
    int size = mapBox.length;
    //Declaracion de iterador
    int i = 0;
    List<String> places = [];
    //Inicio del loop
    while (i < size) {
      //Se continua con el loop
      String locationActual = mapBox.getAt(i);
      //Parsear dobles

      places.add(locationActual);
      i++;
    }
    return places;
  }

  void seleccionarFecha() async {
    DateTime actual = DateTime.now();
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.utc(actual.year, actual.month, 1),
        firstDate: DateTime.utc(actual.year, actual.month, 1),
        lastDate: DateTime(2100));

    if (date != null) {
      final TimeOfDay? hora =
          // ignore: use_build_context_synchronously
          await showTimePicker(context: context, initialTime: TimeOfDay.now());

      if (hora != null) {
        final fechaFinal =
            DateTime(date.year, date.month, date.day, hora.hour, hora.minute);

        fecha = Timestamp.fromDate(fechaFinal);
      }
    }
  }

  String? dropTableValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "Please select an option";
    }
    return null;
  }

  //Valida que la lista the lugares no está vacia
  String? dropTableValidatorPlaces(String? value) {
    if (value == null || value.isEmpty) {
      //Se verifica si hay places creados
      if (places.isEmpty == true) {
        return "Please create a place first";
      }
      return "Please select an option";
    }
    return null;
  }

  //Valida que la lista the lugares no está vacia
  String? dropTableValidatorTeams(String? value) {
    if (value == null || value.isEmpty) {
      //Se verifica si hay matches creados
      if (teamOptions.isEmpty == true) {
        return "Please create a Team first";
      }
      return "Please select an option";
    }
    return null;
  }

//Dialogo de creaacion de equipo correcta
  _showDialog(BuildContext context, String result, String place, String name) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Match Details"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content:
            Text("You played with team $name and $result in place: $place"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.matches);
            },
            child: const Text("Go to Matches"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Dialogo de no crear un team con las cosas
  // ignore: unused_element
  _showDuplicateDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Place created"),
            Icon(
              Icons.warning,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("You already added this location before!"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Image of the Screen view
  Widget mascot() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'lib/img/matches_image.jpg',
        height: 150,
        width: 150,
      ),
    );
  }

//Generic fallback screen
  Widget genericFallback() {
    return Column(
      children: [
        const SizedBox(height: 10),
        const SizedBox(height: 10),
        Image.asset('person.png'),
        const SizedBox(height: 10),
        Align(
            alignment: Alignment.bottomCenter,
            child: Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                color: const Color.fromRGBO(229, 36, 55, 1),
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
                    child: const Center(
                        child: Text(
                            "Oops, seems like something happened! restart the page.",
                            style: TextStyle(
                                fontSize: 20, color: Colors.white)))))),
      ],
    );
  }
}
