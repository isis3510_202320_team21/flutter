import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/routine.dart';
import 'package:teamup/provider/routines_provider.dart';

// ignore: must_be_immutable
class ListRoutineScreen extends StatelessWidget {
  final List<MyRoutine> routines;

  ListRoutineScreen(this.routines, {super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: RoutinesProvider())],
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
          body: Column(
            children: [buildList(context)],
          ),
        ));
  }

  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  Future<void> uploadToShared(BuildContext context, MyRoutine rout) async {
    await Provider.of<RoutinesProvider>(context, listen: false)
        .addToFireBase(rout);
  }

  Widget buildList(BuildContext context) {
    return Expanded(
        child: ListView.builder(
            itemCount: routines.length,
            itemBuilder: ((context, index) {
              final item = routines[index];
              return buildBlock(context, item);
            })));
  }

  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  //Event log of screens for realtime dashborad in Firebase Analytics.
  void logEventRoutine(String routineName, String rate) {
    analytics.logEvent(name: "RoutineShared", parameters: {
      "RoutineName": routineName,
      "RoutineRate": rate
    }).then((_) => null);
  }

  Widget buildBlock(BuildContext context, MyRoutine routine) {
    String media;
    if (routine.images.isEmpty) {
      media = "https://wger.de/media/exercise-images/91/Crunches-1.png";
    } else {
      media = routine.images[0];
    }
    String muscles = "Not specified";
    if (routine.muscles.isNotEmpty) {
      muscles =
          routine.muscles.toString().replaceAll("[", "").replaceAll("]", "");
    }
    String secMuscles = "Not specified";
    if (routine.musclesSecondary.isNotEmpty) {
      secMuscles = routine.musclesSecondary
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
    String equipment = "Not specified";
    if (routine.equipment.isNotEmpty) {
      equipment =
          routine.equipment.toString().replaceAll("[", "").replaceAll("]", "");
    }

    return Container(
      color: Colors.black,
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          color: const Color.fromARGB(255, 98, 98, 98),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // header
              Container(
                padding: const EdgeInsetsDirectional.only(
                    start: 20, end: 20, top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              routine.author,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.white,
                              ),
                            ),
                            RichText(
                              text: const TextSpan(
                                children: [
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.location_on_outlined,
                                      size: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "USA",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            logEventRoutine(
                                routine.name, routine.rate.toString());
                            uploadToShared(context, routine).then((_) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                content: Text('The routine has been shared!'),
                              ));
                            });
                          },
                          child: const Icon(
                            Icons.share,
                            size: 25,
                            color: Colors.white,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              // content
              GestureDetector(
                onDoubleTap: () {
                  // ignore: todo
                  // TODO: show like effect
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Stack(
                    children: [
                      const SizedBox(
                        width: 700,
                        height: 370,
                      ),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(35),
                          child: SizedBox(
                            width: 700,
                            height: 350,
                            child: Image.network(
                              media,
                              fit: BoxFit.cover,
                            ),
                          )),
                      Positioned(
                        bottom: -3,
                        left: 50,
                        right: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Container(
                            height: 50,
                            color: const Color.fromRGBO(245, 184, 0, 1),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  routine.category,
                                  textAlign: TextAlign.center,
                                  textScaleFactor: 2,
                                  style: const TextStyle(
                                      fontFamily: 'BungeeInLine',
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              const SizedBox(
                height: 5,
              ),

              // // actions
              // const PostActionsWidget(),
              // likes
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "${routine.name}: "),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "Category: ${routine.category} "),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.white),
                    children: [
                      TextSpan(text: "created ${routine.creationDate}"),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Description",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              // description
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: routine.description,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Muscles:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: muscles,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Secondary muscles:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: secMuscles,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      // hashtags
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "Equipment:",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                    children: [
                      // username
                      TextSpan(
                        text: equipment,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              // comments
            ],
          ),
        ),
      ),
    );
  }
}
