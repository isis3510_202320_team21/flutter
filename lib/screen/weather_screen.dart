import 'dart:convert';
import 'dart:math';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:teamup/Services/service_broker.dart';
import 'package:vector_math/vector_math.dart' as math;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../model/weather.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

class WeatherScreen extends StatefulWidget {
  @override
  const WeatherScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const WeatherScreen();
  @override
  // ignore: library_private_types_in_public_api
  _WeatherScreen createState() => _WeatherScreen();
}

class _WeatherScreen extends State<WeatherScreen> {
// Se declara la variable weather
  Weather? data = Weather();
//Se declara variable de ubicacion
  late double latitude;
  late double longitude;
  //Se obtiene Service Broker
  ServiceBroker serviceBroker = ServiceBroker();

  //Se obtiene la distancia menor del tm
  late double menor;
  //Se incializar
  void initialize() {
    latitude = 4.60296;
    longitude = -74.06521;
  }

  //Metodo para encontrar el clima actual basado en los datos
  Future<Weather?> getData() async {
    return data = await serviceBroker.getCurrentWeather();
  }

  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  // Build widget for the screen
  @override
  Widget build(BuildContext context) {
    _determinePosition();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Weather"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: Center(
          child: FutureBuilder(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //Create Weather widget
                  Center(child: currentWeather()),
                ],
              );
            } else {
              return genericFallback();
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return const Center(
              child: Text("No internet connection"),
            );
          }
        },
      )),
    );
  }

  Widget currentWeather() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
              child: Container(
            padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
            child: Material(
              elevation: 5,
              borderRadius: BorderRadius.circular(30),
              color: Colors.white,
              child: Center(
                  child: Column(
                children: [
                  const SizedBox(height: 30),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.location_on_outlined),
                      Text('Bogota'),
                    ],
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    DateFormat.yMMMEd().add_jm().format(DateTime.now()),
                  ),
                  const SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(width: 16.0),
                      Text(
                        '${data?.temp}°C',
                        style: const TextStyle(
                          fontSize: 55,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  CachedNetworkImage(
                    imageUrl:
                        'https://openweathermap.org/img/wn/${data!.icon}@2x.png',
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    'Feels like ${data!.feelsLike}°C',
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    toBeginningOfSentenceCase('${data!.description}') ?? '',
                  ),
                ],
              )),
            ),
          )),
          const SizedBox(height: 10),
          recommendationWidget(),
          const SizedBox(height: 10),
          mascot(),
        ]);
  }

  Widget mascot() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'lib/img/muñeco.png',
        height: 150,
        width: 150,
      ),
    );
  }

  Widget recommendationWidget() {
    return Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(30),
        color: const Color.fromRGBO(229, 36, 55, 1),
        child: Container(
            margin: const EdgeInsets.all(5.0),
            padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
            child: FutureBuilder<String>(
              future:
                  getRecommendation(), // a previously-obtained Future<String> or null
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                List<Widget> children;
                if (snapshot.hasData) {
                  children = <Widget>[
                    Center(
                        child: Text(
                      '${snapshot.data}',
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.white),
                    )),
                    const SizedBox(height: 10),
                    locationRecommendationWidget(),
                  ];
                } else if (snapshot.hasError) {
                  children = <Widget>[
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                        'Please check your internet connection and try again',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ];
                } else {
                  children = const <Widget>[
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: CircularProgressIndicator(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Center(
                          child: Text(
                        'Loading recommendation...',
                        style: TextStyle(color: Colors.white),
                      )),
                    ),
                  ];
                }
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: children,
                  ),
                );
              },
            )));
  }

  //Widget for second recomendation
  Widget locationRecommendationWidget() {
    return Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(30),
        color: const Color.fromRGBO(229, 36, 55, 1),
        child: Container(
            margin: const EdgeInsets.all(5.0),
            padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
            child: FutureBuilder<String>(
              future:
                  getEstaciones(), // a previously-obtained Future<String> or null
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                List<Widget> children;
                if (snapshot.hasData) {
                  children = <Widget>[
                    Center(
                        child: Text(
                      '${snapshot.data}',
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.white),
                    )),
                  ];
                } else if (snapshot.hasError) {
                  children = <Widget>[
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                        'Please check your internet connection and try again',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ];
                } else {
                  children = const <Widget>[
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: CircularProgressIndicator(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Center(
                          child: Text(
                        'Loading recommendation...',
                        style: TextStyle(color: Colors.white),
                      )),
                    ),
                  ];
                }
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: children,
                  ),
                );
              },
            )));
  }

  Widget genericFallback() {
    return Column(
      children: [
        const SizedBox(height: 10),
        const SizedBox(height: 10),
        Image.asset('lib/img/no-wifi-icon.jpg'),
        const SizedBox(height: 10),
        Align(
            alignment: Alignment.bottomCenter,
            child: Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                color: const Color.fromRGBO(229, 36, 55, 1),
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
                    child: const Center(
                        child: Text(
                            "No internet, restart the forecast page as soon you get connection.",
                            style: TextStyle(
                                fontSize: 30, color: Colors.white)))))),
      ],
    );
  }

//Metodo para encontrar la ubicacion actual del usuario
  Future<void> _determinePosition() async {
    LocationData? currentLocation;
    var location = Location();
    try {
      // Find and store your location in a variable
      currentLocation = await location.getLocation();
      latitude = currentLocation.latitude!;
      longitude = currentLocation.longitude!;
    } on Exception {
      //Log crashlytics
      await FirebaseCrashlytics.instance.recordError('Location error', null);
    }
  }

  Future<String> getRecommendation() async {
    //Se consigue la instancia del clima con la ubicacion actual
    Weather? currentData =
        await serviceBroker.getCurrentWeatherFromLocation(latitude, longitude);
    //Instancias de variables
    String? main = currentData.main;
    String? description = currentData.description;

    String recommendation =
        'Error fetching data, please check your internet connection';
    if (main == 'Clear') {
      recommendation =
          "It's a good time to go out and practice outdoor sports, don't forget to take your sunblock!";
    } else if (main == 'Clouds') {
      recommendation =
          "It seems you can practice outdoor sports, be aware of rain because there are $description.";
    } else {
      recommendation =
          "Maybe it's not the time to practice outdoor sports because the weather is $main, you should try to find an indoor facility to play!";
    }
    return recommendation;
  }

  //Servicio que retorna la estación de transmilenio más cercana
  Future<String> getEstaciones() async {
    //Fallback message
    String resultado = "Error loading";
    //Llamado del service provider
    String responseBody = await serviceBroker.getEstaciones();
    //Calculo de proximidad y algoritmo del menor
    Map<String, dynamic> estacionMap =
        await getEstacionMasCercana(responseBody);
    return recomendacionEstacion(resultado, estacionMap);
  }

  String recomendacionEstacion(
      String resultado, Map<String, dynamic> estacionMap) {
    //Variables para concatenar la respuesta.
    String nombreEstacion = estacionMap["nombre_estacion"];
    String ubicacionEstacion = estacionMap["ubicacion_estacion"];

    if (estacionMap["biciestacion_estacion"] != 0) {
      resultado =
          "Your nearest transmilenio station is $nombreEstacion and it is $menor KM at $ubicacionEstacion.";
    } else {
      int capacidad =
          estacionMap["capacidad_biciestacion_estacion"].toInteger();
      resultado =
          "Your nearest transmilenio station is $nombreEstacion, it is $menor KM  at $ubicacionEstacion and has a capacity for $capacidad bicycles.";
    }
    return resultado;
  }

  Future<Map<String, dynamic>> getEstacionMasCercana(
      String responseBody) async {
//Declaración del iterador
    int i = 0;
    //Se obtiene el tamaño de la respuesta
    List estacion = jsonDecode(responseBody)['features'];
    int length = estacion.length;
    //Declaración de la variable para el algoritmo mayor
    Map<String, dynamic> estacionMap =
        jsonDecode(responseBody)['features'][0]['attributes'];
    //Declaracion para el algoritmo
    menor = 99999;
    while (i < length) {
      //Se obtiene la estación en particular
      Map<String, dynamic> estacionActual =
          jsonDecode(responseBody)['features'][i]['attributes'];
      //Se obtienen los valores actuales de la latitud y la longitud
      double latitudeActual = estacionActual['latitud_estacion'].toDouble();
      double longitudActual = estacionActual['longitud_estacion'].toDouble();
      //Se calcula la distancia entre coordenadas
      double actual = calculateDistance(
          latitude, longitude, latitudeActual, longitudActual);
      //Se realiza la comparación del algoritmo
      if (menor > actual) {
        menor = actual;
        estacionMap = estacionActual;
      }
      i++;
    }
    //Se declara menor en el formato establecido
    menor = menor.round().toDouble();
    return estacionMap;
  }

  //Método para calcular la proximidad de la ubicación actual con la estación
  //Implementar para los equipos de esta forma, someproperty seria el team
  //someObjects.sort((a, b) => a.someProperty.compareTo(b.someProperty));
  //Metodo para calcular las distancias
  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    int radiusEarth = 6371;
    double distanceKm;
    double dlat, dlng;
    double a;
    double c;

    //Convertimos de grados a radianes
    lat1 = math.radians(lat1);
    lat2 = math.radians(lat2);
    lng1 = math.radians(lng1);
    lng2 = math.radians(lng2);
    // Fórmula del semiverseno
    dlat = lat2 - lat1;
    dlng = lng2 - lng1;
    a = sin(dlat / 2) * sin(dlat / 2) +
        cos(lat1) * cos(lat2) * (sin(dlng / 2)) * (sin(dlng / 2));
    c = 2 * atan2(sqrt(a), sqrt(1 - a));

    distanceKm = radiusEarth * c;

    return distanceKm;
  }
}
