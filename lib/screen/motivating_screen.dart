import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/model/matches.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/matches_provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:cached_video_player/cached_video_player.dart';

class MotivatingScreen extends StatefulWidget {
  const MotivatingScreen({super.key});
  static Widget create(BuildContext context) => const MotivatingScreen();
  @override
  State<MotivatingScreen> createState() => _MotivatingScreenState();
}

class _MotivatingScreenState extends State<MotivatingScreen> {
  late CachedVideoPlayerController controller;
  @override
  void initState() {
    controller = CachedVideoPlayerController.network(
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4");
    controller.initialize().then((value) {
      controller.play();
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MotivatingScreen object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text('Sponsored by: Art School'),
      ),
      body: Center(
          child: controller.value.isInitialized
              ? AspectRatio(
                  aspectRatio: controller.value.aspectRatio,
                  child: CachedVideoPlayer(controller))
              : const CircularProgressIndicator()), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void dispose() {
    controller.setVolume(0.0);
    super.dispose();
  }
}
