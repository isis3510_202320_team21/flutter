import 'package:connectivity/connectivity.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:teamup/navigation/routes.dart';
import '../../cubit/cubit_auth.dart';
import '../../cubit/state_auth.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  static Widget create(BuildContext context) => const RegisterScreen();

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final nameController = TextEditingController();
    final emailController = TextEditingController();
    final passwordController = TextEditingController();
    final repeatPasswordController = TextEditingController();
    final ageController = TextEditingController();
    final descriptionController = TextEditingController();

    final myBox = Hive.box("register");

    // ignore: non_constant_identifier_names
    void StoreLocaly() {
      Hive.openBox("register");
      myBox.put(1, nameController.text);
      myBox.put(2, emailController.text);
      myBox.put(3, passwordController.text);
      myBox.put(4, ageController.text);
      myBox.put(5, descriptionController.text);
    }

    // ignore: non_constant_identifier_names
    void ClearLocaly() {
      Hive.openBox("register");
      myBox.delete(1);
      myBox.delete(2);
      myBox.delete(3);
      myBox.delete(4);
      myBox.delete(5);
    }

    // ignore: non_constant_identifier_names
    void LoadLocal() {
      Hive.openBox("register");
      nameController.text = myBox.get(1, defaultValue: '');
      emailController.text = myBox.get(2, defaultValue: '');
      passwordController.text = myBox.get(3, defaultValue: '');
      ageController.text = myBox.get(4, defaultValue: '');
      descriptionController.text = myBox.get(5, defaultValue: '');
    }

    LoadLocal();

    final isSigningIn = context.watch<AuthCubit>().state is AuthSigningIn;

    String? emailValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      if (!EmailValidator.validate(value)) return 'Enter a valid email';
      return null;
    }

    String? passwordValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      if (value.length < 6) return 'Password should be at least 6 letters';
      if (passwordController.text != repeatPasswordController.text) {
        return 'Password do not match';
      }
      return null;
    }

    String? requiredValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      return null;
    }

    // ignore: no_leading_underscores_for_local_identifiers
    _showInternetDialog(BuildContext context) {
      showDialog(
        builder: (context) => CupertinoAlertDialog(
          title: const Column(
            children: <Widget>[
              Text("Internet error registering user"),
              Icon(
                Icons.warning,
                color: Color.fromRGBO(245, 184, 0, 1),
              ),
            ],
          ),
          content: const Text("Please enable your internet and try again."),
          actions: <Widget>[
            CupertinoDialogAction(
              isDestructiveAction: true,
              child: const Text("Close",
                  style:
                      TextStyle(color: Color.fromRGBO(245, 184, 0, 1))),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        context: context,
      );
    }

    return AbsorbPointer(
      absorbing: isSigningIn,
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        appBar: AppBar(
          title: const Text('Register User',
              style: TextStyle(color: Colors.black)),
          backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
        ),
        body: BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            return Container(
              padding: const EdgeInsets.all(24),
              child: Center(
                child: ListView(
                  children: [
                    Form(
                      key: formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: nameController,
                            maxLength: 30,
                            decoration: const InputDecoration(
                                labelText: 'Full Name',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            style: const TextStyle(color: Colors.white),
                          ),
                          TextFormField(
                            controller: emailController,
                            maxLength: 30,
                            decoration: const InputDecoration(
                                labelText: 'Email',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: emailValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            style: const TextStyle(color: Colors.white),
                          ),
                          TextFormField(
                            controller: passwordController,
                            maxLength: 30,
                            decoration: const InputDecoration(
                                labelText: 'Password',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: passwordValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            obscureText: true,
                            style: const TextStyle(color: Colors.white),
                          ),
                          TextFormField(
                            controller: repeatPasswordController,
                            maxLength: 30,
                            decoration: const InputDecoration(
                                labelText: 'Repet password',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: passwordValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            obscureText: true,
                            style: const TextStyle(color: Colors.white),
                          ),
                          TextFormField(
                            controller: ageController,
                            maxLength: 3,
                            decoration: const InputDecoration(
                                labelText: 'Age',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            keyboardType: TextInputType.number,
                            style: const TextStyle(color: Colors.white),
                          ),
                          TextFormField(
                            controller: descriptionController,
                            maxLength: 500,
                            decoration: const InputDecoration(
                                labelText: 'Description',
                                labelStyle: TextStyle(color: Colors.white)),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    if (isSigningIn)
                      Container(
                        alignment: Alignment.center,
                        width: 50,
                        child: const CircularProgressIndicator(),
                      ),
                    if (state is AuthError)
                      Text(
                        state.message,
                        style: const TextStyle(
                            color: Colors.black,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.black, backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                        textStyle: const TextStyle(
                          color: Colors.black,
                          fontSize: 30,
                        ),
                        minimumSize: const Size.fromHeight(50),
                      ),
                      child: const Text('Register'),
                      onPressed: () {
                        if (formKey.currentState?.validate() == true) {
                          context.read<AuthCubit>().registerUser(
                                nameController.text,
                                emailController.text,
                                passwordController.text,
                                int.tryParse(ageController.text) ?? 0,
                                descriptionController.text,
                              );
                          var connectivityResult =
                              (Connectivity().checkConnectivity());
                          // ignore: unrelated_type_equality_checks
                          if (connectivityResult == ConnectivityResult.mobile ||
                              // ignore: unrelated_type_equality_checks
                              connectivityResult == ConnectivityResult.wifi) {
                            ClearLocaly();
                          } else {
                            _showInternetDialog(context);
                          }
                        }
                      },
                    ),
                    Row(
                      // ignore: sort_child_properties_last
                      children: <Widget>[
                        const Text(
                          'Already have an account?',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextButton(
                          onPressed: () {
                            context.read<AuthCubit>().reset();
                            Navigator.pushNamed(context, Routes.login);
                          },
                          child: const Text(
                            'LogIn',
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
