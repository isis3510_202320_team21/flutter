import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teamup/database/shared_preferences.dart';
import 'package:teamup/provider/matches_provider.dart';

import '../navigation/routes.dart';

class MatchesScreen extends StatefulWidget {
  @override
  const MatchesScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const MatchesScreen();

  @override
  // ignore: library_private_types_in_public_api
  _MatchesScreen createState() => _MatchesScreen();
}

class _MatchesScreen extends State<MatchesScreen> {
  SharedPrefs prefs = SharedPrefs();
  List<String> matches = List.empty();
  @override
  void dispose() {
    super.dispose();
  }

  //Lo que selecciona el bottomAppBar
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    prefs.sharePrefsInit();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Matches"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: Center(
          child: FutureBuilder(
              future: getMatches(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //Create Weather widget
                      Center(child: MatchesList()),
                    ],
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              })),
      floatingActionButton: FloatingActionButton.extended(
        label: const Text('Clear Places list'),
        icon: const Icon(Icons.delete),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
        onPressed: () {
          //Se elimina de hive los lugares
          _showDeleteConfirmationDialog(context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),
            label: 'Track',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star_border_outlined),
            label: 'Community Matches',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.share),
            label: 'Share Matches',
            backgroundColor: Color.fromRGBO(229, 36, 55, 1),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromRGBO(229, 36, 55, 1),
        onTap: _onItemTapped,
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Widget MatchesList() {
//Se obtienen los lugares para el dropdown
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: matches.length,
        itemBuilder: (context, index) {
          List<String> match = matches[index].split(',');
          String name = match[0];
          String result = match[1];
          String place = "${match[2]},${match[3]}";
          String date = match[4];

          return ListTile(
              title: Text("Match with $name at $date"),
              subtitle: Text(
                result,
                style: const TextStyle(color: Colors.green),
              ),
              onTap: () {
                _showDialog(context, result, place, name, date);
              });
        });
  }

  //Metodo para obtener las partidas creadas
  Future<List<String>> getMatches() async {
    matches = await prefs.getMatches();
    return matches;
  }

  //Pop up con detalle del partido
  _showDialog(BuildContext context, String result, String place, String name,
      String date) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Match Details"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: Text(
            "You played with team $name and $result at $date in place: $place"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.tracking);
            },
            child: const Text("Go to Tracking"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

  _showDeleteConfirmationDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("WARNING"),
            Icon(
              Icons.warning_amber_outlined,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text(
            "Are you sure you want to delete all your matches? This will erase your matches information forever."),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              //Se eliminan los equipos
              prefs.deleteMatches();
              //Se cierra el dialogo
              Navigator.of(context).pop();
              //Se actualiza la pagina
              setState(() {});
            },
            child: const Text("Delete"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

//Pop up con detalle del partido
  _showDialogCommunity(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Thank you!"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content:
            const Text("We shared your favorite Matches in the community!"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.tracking);
            },
            child: const Text("Go to Tracking!"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

  // metodo que maneja las acciones de la navbar
  Future<void> _onItemTapped(int index) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        _selectedIndex = index;

        if (_selectedIndex == 0) [];
        if (_selectedIndex == 1) {
          Navigator.pushNamed(context, Routes.communityMatches);
        }
        if (_selectedIndex == 2) {
          MatchesProvider().addMatches();
          _showDialogCommunity(context);
        }
      });
    } else {
      // ignore: use_build_context_synchronously
      _showInternetDialog(context);
      // Not connected to any network.
    }
  }

  //Internet dialog
  _showInternetDialog(BuildContext context) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("No internet connection found!"),
            Icon(
              Icons.warning,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: const Text("Please enable your internet and try again."),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }
}
