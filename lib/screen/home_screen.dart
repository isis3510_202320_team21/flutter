import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teamup/cubit/cubit_auth.dart';
import 'package:teamup/cubit/user_cubit.dart';
import 'package:teamup/model/user.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/repository/implementation/user_repository.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => UserCubit(UserRepository())..getUser(),
      child: const HomeScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserReadyState) {
            return HomeBuild(state.user);
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class HomeBuild extends StatefulWidget {
  final MyUser user;

  const HomeBuild(this.user, {super.key});

  @override
  State<HomeBuild> createState() => _HomeBuildState();
}

class _HomeBuildState extends State<HomeBuild> {
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  //Event log of screens for realtime dashborad in Firebase Analytics.
  void logEventScreen(String screenName, String x) {
    analytics.logEvent(name: "menu_nav", parameters: {
      "buttonscreen": screenName,
    }).then((_) => null);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.user.image != null && widget.user.image!.isNotEmpty) {}

    return Scaffold(
      appBar: AppBar(
        leading: Center(
          child: ClipOval(
            child: SizedBox(
              child: IconButton(
                icon: const Icon(Icons.person),
                onPressed: () {
                  Navigator.pushNamed(context, Routes.profile);
                },
              ),
            ),
          ),
        ),
        title:
            Text(widget.user.name, style: const TextStyle(color: Colors.black)),
        backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, Routes.settings);
              logEventScreen("SettingsScreen", "Views");
            },
            icon: const Icon(Icons.settings),
            color: Colors.black,
          ),
          IconButton(
            onPressed: () => context.read<AuthCubit>().signOut(),
            icon: const Icon(Icons.logout),
            color: Colors.black,
          ),
        ],
      ),
      body: BlocBuilder<UserCubit, UserState>(builder: (context, state) {
        return Container(
            padding: const EdgeInsets.all(24),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'TEAM UP',
                      textAlign: TextAlign.center,
                      textScaleFactor: 3,
                      style: TextStyle(
                        color: Color.fromRGBO(245, 184, 0, 1),
                        fontFamily: 'BungeeInline',
                      ),
                    ),
                    Image.asset(
                      'lib/img/muñeco.png',
                      height: 80,
                      width: 80,
                    ),
                    const Padding(padding: EdgeInsets.all(20.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              const Color.fromRGBO(229, 36, 55, 50),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('MAP'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.map);
                          logEventScreen("MapScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('TEAMS'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.teams);
                          logEventScreen("TeamScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              const Color.fromRGBO(229, 36, 55, 50),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('SPORTS'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.sports);
                          logEventScreen("SportsScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('PLACES'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.places);
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              const Color.fromRGBO(229, 36, 55, 50),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('FORECAST'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.weather);
                          logEventScreen("WeatherScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('TRACK'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.tracking);
                          logEventScreen("TrackScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              const Color.fromRGBO(229, 36, 55, 50),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('ROUTINES'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.routines);
                          logEventScreen("RoutineScreen", "Views");
                        }),
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(245, 184, 0, 1),
                          textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('TOURNAMENTS'),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.tournaments);
                          logEventScreen("TournamentsScreen", "Views");
                        })
                  ],
                ),
              ),
            ));
      }),
    );
  }
}
