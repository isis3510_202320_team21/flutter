import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamup/navigation/routes.dart';
import 'package:teamup/provider/teams_provider.dart';
import 'package:teamup/screen/team_detail_screen.dart';
import '../model/team.dart';
import 'package:connectivity/connectivity.dart';

class TeamsScreen extends StatefulWidget {
  const TeamsScreen({super.key});

  static Widget create(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: TeamsProvider())],
        child: const TeamsScreen());
  }

  @override
  State<StatefulWidget> createState() => _TeamsScreenState();
}

class _TeamsScreenState extends State<TeamsScreen> {
  final teamSearchController = TextEditingController();
  List<MyTeam> listaFiltrada = List.empty();

  void _navegarADetalle(BuildContext context, MyTeam myTeam) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => team_detail_screen(myTeam),
      ),
    );
  }

  void asignarLista(BuildContext context) {
    Future<List<MyTeam>> lista = Provider.of<TeamsProvider>(context).getTeams();
    lista.then((value) => listaFiltrada = value);
  }

//Metodo que verifica la conectividad
  Future<bool> messageNet(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // Not connected to any network.
    }
  }

  @override
  Widget build(BuildContext context) {
    bool xd;
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
          actions: [
            IconButton(
                onPressed: () => Navigator.pushNamed(context, Routes.settings),
                icon: const Icon(Icons.list),
                color: Colors.white),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(24),
          child: Center(
              child: ListView(children: [
            const SizedBox(
              height: 75,
              width: double.infinity,
              child: RepaintBoundary(
                  child: Text(
                'Your teams',
                textAlign: TextAlign.center,
                textScaleFactor: 3,
                style:
                    TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
              )),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              height: 16,
            ),

            Container(
                height: 85,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(229, 36, 55, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextFormField(
                  controller: teamSearchController,
                  maxLength: 30,
                  decoration: const InputDecoration(
                      labelText: 'Search by name or sport',
                      labelStyle: TextStyle(color: Colors.black)),
                )),

            buildTeamsList(context),

            const SizedBox(
              height: 16,
            ),
            Container(
              height: 85,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(229, 36, 55, 1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromRGBO(229, 36, 55, 1)),
                  onPressed: () async => {
                      
                        xd = await messageNet(context),
                        if (xd == true)
                          {Navigator.pushNamed(context, Routes.createTeam)}
                        else
                          {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('Offline'),
                              ),
                            )
                          }
                      },
                  child: const Text(
                    'CREATE TEAM',
                    textAlign: TextAlign.center,
                    textScaleFactor: 2,
                    style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Share Tech',
                    ),
                  )),
            )
            //Aqui deberia haber padding
          ])),
        ));
  }

  Widget buildTeamsList(BuildContext context) {
    asignarLista(context);
    if (listaFiltrada.isEmpty) {
      Widget t = Container();
      Stream<bool> mensaje = Stream.fromFuture(messageNet(context));
      // ignore: unrelated_type_equality_checks
      if (mensaje == false) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Check your internet connection'),
          backgroundColor: Color.fromRGBO(229, 36, 55, 1),
        ));
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Loading teams...",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            )
          ],
        );
      } else {
        t = const Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Text(
              "Check your internet connection... ",
              textAlign: TextAlign.center,
              textScaleFactor: 2,
              style: TextStyle(fontFamily: 'BungeeInLine', color: Colors.white),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 40, // Establece el ancho del indicador
              height: 40, // Establece la altura del indicador
              child: CircularProgressIndicator(
                strokeWidth: 4, // Cambia el tamaño del indicador
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.blue), // Cambia el color del indicador
              ),
            )
          ],
        );
      }
      return t;
    } else {
      return SizedBox(
        height: 400,
        width: double.infinity,
        child: ListView.builder(
            itemCount: listaFiltrada.length,
            itemBuilder: ((context, index) {
              final item = listaFiltrada[index];

              Color color = const Color.fromRGBO(229, 36, 55, 1);
              DateTime fecha = item.date.toDate();
              if ((item.name.contains(teamSearchController.text) ||
                  item.sport.contains(teamSearchController.text))) {
                if (fecha.day == DateTime.now().day) {
                  color = const Color.fromARGB(255, 43, 186, 0);
                }

                return SizedBox(
                  height: 130,
                  width: double.infinity,
                  child: RepaintBoundary(
                    child: Center(
                        child: Column(children: [
                      const SizedBox(
                        height: 16,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: color,
                        ),
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: color,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text(
                                item.name,
                                textAlign: TextAlign.center,
                                textScaleFactor: 2,
                                style: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                ),
                              ),
                            ),
                            Text("${item.members} members",
                                textAlign: TextAlign.center,
                                textScaleFactor: 2,
                                style: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontFamily: 'Share Tech',
                                )),
                          ],
                        ),
                        onPressed: () => _navegarADetalle(context, item),
                      ),
                    ])),
                  ),
                );
              } else {
                return const SizedBox(
                  height: 2,
                );
              }
            })),
      );
    }
  }
}
