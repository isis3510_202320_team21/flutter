import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:teamup/database/shared_preferences.dart';
import 'package:teamup/screen/community_screen.dart';

import '../model/team.dart';
import '../navigation/routes.dart';

class TrackScreen extends StatefulWidget {
  @override
  const TrackScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const TrackScreen();

  @override
  // ignore: library_private_types_in_public_api
  _TrackScreen createState() => _TrackScreen();
}

class _TrackScreen extends State<TrackScreen> {
  //Instanciacion de variables
  SharedPrefs prefs = SharedPrefs();
  Timestamp fecha = Timestamp.now();
  Timestamp base = Timestamp.now();
  //Se declaran arreglos de precargados de equipos
  // ignore: non_constant_identifier_names
  List<MyTeam> lista_filtrada = List.empty();
  // ignore: non_constant_identifier_names
  List<String> team_options = List.empty(growable: true);
  // ignore: non_constant_identifier_names
  List<String> win_options = ["Win", "Loss"];
  List<String> places = List.empty();
  List<_ChartData>? chartDataWon;
  List<_ChartData>? chartDataLost;
  String selectedPlace = "Uniandes";
  String selectedTeam = "Team";
  String selectedResult = "w";
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    chartDataWon = <_ChartData>[
      _ChartData(2005, 21, 28),
      _ChartData(2006, 24, 44),
      _ChartData(2007, 36, 48),
      _ChartData(2008, 38, 50),
      _ChartData(2009, 54, 66),
      _ChartData(2010, 57, 78),
      _ChartData(2011, 70, 84)
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //Se pide la instancia de las shared preferences
    prefs.sharePrefsInit();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Tracking"),
        backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
      ),
      body: FutureBuilder(
        future: filterMatchesDataWon(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //Create Weather widget
                  Center(child: createTrackMenu()),
                ],
              );
            } else {
              return genericFallback();
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return const Center(
              child: Text("No internet connection"),
            );
          }
        },
      ),
    );
  }

  Widget createTrackMenu() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Form(
          key: formKey,
          child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: [
              const SizedBox(
                height: 16,
              ),
              _buildDefaultLineChart(),
              const SizedBox(
                height: 8,
              ),
              const SizedBox(
                height: 8,
              ),
              const SizedBox(
                height: 16,
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, Routes.matches);
                  },
                  child: const Text('      Match List       ')),
              const SizedBox(
                height: 16,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                  textStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, Routes.match);
                },
                child: const Text('      Create Match     '),
              ),
              const SizedBox(
                height: 16,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromRGBO(229, 36, 55, 1),
                  textStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const CommunityScreen(),
                      ));
                },
                child: const Text('      Go to Comunity!     '),
              ),
            ],
          )),
    );
  }

  // ignore: non_constant_identifier_names
  Widget MatchesList() {
//Se obtienen los lugares para el dropdown
    List<String> matches = getMatches();
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: matches.length,
        itemBuilder: (context, index) {
          List<String> match = matches[index].split(',');
          String name = match[0];
          String result = match[1];
          String place = "${match[2]},${match[3]}";
          String date = match[4];

          return ListTile(
              title: Text("Match with $name at $date"),
              subtitle: Text(
                result,
                style: const TextStyle(color: Colors.green),
              ),
              onTap: () {
                _showDialog(context, result, place, name, date);
              });
        });
  }

  //Metodo para obtener las partidas creadas
  getMatches() async {
    return await prefs.getMatches();
  }

//Metodo que obtiene los partidos ganados
  getMatchesWon() async {
    return await prefs.getMatchesWon();
  }

//Metodo que obtiene los partidos perdidos
  getMatchesLost() async {
    return await prefs.getMatchesLost();
  }

//Metodo que setea las distintas fechas
  setDateMostWins(String date, int wins) async {
    await prefs.setDateMostWins(date, wins);
  }

  //Metodo que obtiene las victorias por fecha
  Future<int?> getDateMostWins(String date) async {
    return await prefs.getDateMostWins(date);
  }

  //Metodo que setea las distintas fechas
  setDateMostLost(String date, int wins) async {
    await prefs.setDateMostLost(date, wins);
  }

  //Metodo que obtiene las victorias por fecha
  Future<int?> getDateMostLost(String date) async {
    return await prefs.getDateMostLost(date);
  }

//Build matches weekly data
  Future<List<_ChartData>?> filterMatchesDataWon() async {
    //Se obtiene los partidos ganados
    List<String> matchesWon = await getMatchesWon();
    //Numeros de partidos ganados
    int size = matchesWon.length;
    int i = 0;
    List<String> matchesbyDate = [];
    while (i < size) {
      List<String> match = matchesWon[i].split(',');
      String date = match[4];
      //Retorna el numero de victorias del equipo
      int wins = (countMatchesDay(date, matchesWon));
      //Se pone la fecha en el formato deseado
      List<String> dateParsed = date.split(' ');
      String dateValue = dateParsed[1];
      // Se almacena la fecha en el local storage
      setDateMostWins(dateValue, wins);
      //Se guarda la fecha en el arreglo
      matchesbyDate.add(dateValue);
      i++;
    }
    //Se obtienen los datos de  cada una de las fechas
    int j = 0;
    int size2 = matchesbyDate.length;
    //Se vacia la lista previamente creada
    chartDataWon = [];
    while (j < size2) {
      //Se obtiene  el numero de partidos ganados por la fecha actual
      String fechaActual = matchesbyDate[j];
      //Se obtiene el numero de partidos ganados
      int? numGanados = await getDateMostWins(fechaActual);
      //Se obtiene el dia de la fecha del mes en el formato
      List<String> dateParsed = fechaActual.split('/');
      String dayMatch = dateParsed[0];
      //Se transforma a double
      double ganadosFormat = numGanados! + 0.0;
      //Se almacena la fecha actual del mes
      _ChartData dataActual =
          _ChartData(double.parse(dayMatch), ganadosFormat, 0.0);
      //Se verifica si esta duplicado el datoActual
      bool duplicado = duplicateValidation(dataActual);
      if (duplicado == false) {
        //Se agrega al arreglo de datos
        chartDataWon?.add(dataActual);
      }
      //Si esta duplicado no se agrega
      //Se continua con el iterador
      j++;
    }

    //Se ordena en orden ascendente
    ascendingSortDataWin();
    //Se consiguen tambien las perdidas
    chartDataLost = await filterMatchesDataLost();

    return chartDataWon;
  }

//Metodo que verifica si ya existe en el arreglo un elemento con la misma fecha
  bool duplicateValidation(_ChartData dataActual) {
    bool rta = false;
    //Se obtienen los datos de  cada una de las fechas
    int i = 0;
    //Se obtiene el tamanio del arreglo
    int? size = chartDataWon?.length;
    //Se hace el bucle
    while (i < size!) {
      //Se verifica si hay un dato igual ya en el arreglo
      _ChartData actual = chartDataWon![i];
      if (actual.y == dataActual.y && actual.x == dataActual.x) {
        rta = true;
      }
      //Continua el while
      i++;
    }
    return rta;
  }

  //Metodo que verifica si ya existe en el arreglo un elemento con la misma fecha
  bool duplicateValidationLost(_ChartData dataActual) {
    bool rta = false;
    //Se obtienen los datos de  cada una de las fechas
    int i = 0;
    //Se obtiene el tamanio del arreglo
    int? size = chartDataLost?.length;
    //Se hace el bucle
    while (i < size!) {
      //Se verifica si hay un dato igual ya en el arreglo
      _ChartData actual = chartDataLost![i];
      if (actual.y == dataActual.y && actual.x == dataActual.x) {
        rta = true;
      }
      //Continua el while
      i++;
    }
    return rta;
  }

  //Se ordena el arreglo de datos
  void ascendingSortDataWin() {
    //Se obtiene el tamanio del arreglo
    int? size = chartDataWon?.length;
    //Se hace el bucle
    for (int i = 0; i < size!; i++) {
      for (int j = i + 1; j < size; j++) {
        if (chartDataWon![i].x > chartDataWon![j].x) {
          var temp = chartDataWon![i];
          chartDataWon![i] = chartDataWon![j];
          chartDataWon![j] = temp;
        }
      }
    }
  }

  //Se ordena el arreglo de datos
  void ascendingSortDataLost() {
    //Se obtiene el tamanio del arreglo
    int? size = chartDataLost?.length;
    //Se obtiene el mayor
    for (int i = 0; i < size!; i++) {
      for (int j = i + 1; j < size; j++) {
        if (chartDataLost![i].x > chartDataLost![j].x) {
          var temp = chartDataLost![i];
          chartDataLost![i] = chartDataLost![j];
          chartDataLost![j] = temp;
        }
      }
    }
  }

//Build matches weekly data
  Future<List<_ChartData>?> filterMatchesDataLost() async {
    //Se obtiene los partidos perdidos
    List<String> matchesLost = await getMatchesLost();
    //Numeros de partidos perdidos
    int size = matchesLost.length;
    int i = 0;
    List<String> matchesbyDate = [];
    while (i < size) {
      List<String> match = matchesLost[i].split(',');
      String date = match[4];
      //Retorna el numero de victorias del equipo
      int loss = (countMatchesDayLost(date, matchesLost));
      //Se pone la fecha en el formato deseado
      List<String> dateParsed = date.split(' ');
      String dateValue = dateParsed[1];
      // Se almacena la fecha en el local storage
      setDateMostLost(dateValue, loss);
      //Se guarda la fecha en el arreglo
      matchesbyDate.add(dateValue);
      i++;
    }

    //Se obtienen los datos de  cada una de las fechas
    int j = 0;
    int size2 = matchesbyDate.length;
    //Se vacia la lista previamente creada
    chartDataLost = [];
    while (j < size2) {
      //Se obtiene  el numero de partidos ganados por la fecha actual
      String fechaActual = matchesbyDate[j];
      //Se obtiene el numero de partidos Perdidos
      int? numLost = await getDateMostLost(fechaActual);
      //Se obtiene el dia de la fecha del mes en el formato
      List<String> dateParsed = fechaActual.split('/');
      String dayMatch = dateParsed[0];
      //Se transforma a double
      double perdidosFormat = numLost! + 0.0;
      //Se almacena la fecha actual del mes
      _ChartData dataActual =
          _ChartData(double.parse(dayMatch), perdidosFormat, 0.0);
      //Se verifica si esta duplicado el datoActual
      bool duplicado = duplicateValidationLost(dataActual);
      if (duplicado == false) {
        //Se agrega al arreglo de datos
        chartDataLost?.add(dataActual);
      }
      //Si esta duplicado no se agrega
      //Se continua con el iterador
      j++;
    }
    return chartDataLost;
  }

  int countMatchesDay(String date, List<String> matchesWon) {
    //Contador de partidos ganados
    int matchesDay = 0;
    //Se declara el iterador
    int i = 0;
    //Se parsea la fecha
    List<String> dateParsed = date.split(' ');
    String dateValue = dateParsed[1];
    while (i < matchesWon.length) {
      //Se obtiene la fecha por lista
      List<String> matchActual = matchesWon[i].split(',');
      String dateActual = matchActual[4];
      //Se obtiene la fecha en el formato
      List<String> dateActualParsed = dateActual.split(' ');
      String dateActualValue = dateActualParsed[1];
      //Se verifica si es igual a la fecha actual
      if (dateValue == dateActualValue) {
        //Verificar si ya se hizo la cuenta
        matchesDay++;
      }
      i++;
    }
    return matchesDay;
  }

  int countMatchesDayLost(String date, List<String> matchesLost) {
    //Contador de partidos ganados
    int matchesDay = 0;
    //Se declara el iterador
    int i = 0;
    //Se parsea la fecha
    List<String> dateParsed = date.split(' ');
    String dateValue = dateParsed[1];
    while (i < matchesLost.length) {
      //Se obtiene la fecha por lista
      List<String> matchActual = matchesLost[i].split(',');
      String dateActual = matchActual[4];
      //Se obtiene la fecha en el formato
      List<String> dateActualParsed = dateActual.split(' ');
      String dateActualValue = dateActualParsed[1];
      //Se verifica si es igual a la fecha actual
      if (dateValue == dateActualValue) {
        //Verificar si ya se hizo la cuenta
        matchesDay++;
      }
      i++;
    }
    return matchesDay;
  }

  _showDialog(BuildContext context, String result, String place, String name,
      String date) {
    showDialog(
      builder: (context) => CupertinoAlertDialog(
        title: const Column(
          children: <Widget>[
            Text("Match Details"),
            Icon(
              Icons.sports_soccer_sharp,
              color: Color.fromARGB(255, 248, 54, 54),
            ),
          ],
        ),
        content: Text(
            "You played with team $name and $result at $date in place: $place"),
        actions: <Widget>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pushNamed(context, Routes.places);
            },
            child: const Text("Go to places"),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }

  SfCartesianChart _buildDefaultLineChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      title: ChartTitle(text: 'Monthly - Matches'),
      legend:
          Legend(isVisible: true, overflowMode: LegendItemOverflowMode.wrap),
      primaryXAxis: NumericAxis(
          edgeLabelPlacement: EdgeLabelPlacement.shift,
          interval: 2,
          majorGridLines: const MajorGridLines(width: 0)),
      primaryYAxis: NumericAxis(
          labelFormat: '{value}',
          axisLine: const AxisLine(width: 0),
          majorTickLines: const MajorTickLines(color: Colors.transparent)),
      series: _getDefaultLineSeries(),
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  /// The method returns line series to chart.
  List<LineSeries<_ChartData, num>> _getDefaultLineSeries() {
    return <LineSeries<_ChartData, num>>[
      LineSeries<_ChartData, num>(
          animationDuration: 2500,
          dataSource: chartDataWon!,
          xValueMapper: (_ChartData sales, _) => sales.x,
          yValueMapper: (_ChartData sales, _) => sales.y,
          width: 2,
          name: 'Win',
          markerSettings: const MarkerSettings(isVisible: true)),
      LineSeries<_ChartData, num>(
          animationDuration: 2500,
          dataSource: chartDataLost!,
          width: 2,
          name: 'Loss',
          xValueMapper: (_ChartData sales, _) => sales.x,
          yValueMapper: (_ChartData sales, _) => sales.y,
          markerSettings: const MarkerSettings(isVisible: true))
    ];
  }

//Generic fallback screen
  Widget genericFallback() {
    return Column(
      children: [
        const SizedBox(height: 10),
        const SizedBox(height: 10),
        Image.asset('person.png'),
        const SizedBox(height: 10),
        Align(
            alignment: Alignment.bottomCenter,
            child: Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                color: const Color.fromRGBO(229, 36, 55, 1),
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding: const EdgeInsets.fromLTRB(25, 15, 25, 5),
                    child: const Center(
                        child: Text(
                            "Oops, seems like something happened! restart the page.",
                            style: TextStyle(
                                fontSize: 20, color: Colors.white)))))),
      ],
    );
  }
//Method that filles the chart data

  @override
  void dispose() {
    chartDataWon!.clear();
    chartDataLost!.clear();
    super.dispose();
  }
}

class _ChartData {
  _ChartData(this.x, this.y, this.y2);
  final double x;
  final double y;
  final double y2;
}
