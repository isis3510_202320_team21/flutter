import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

List<MyTeam> myTeamFromJson(String str) =>
    List<MyTeam>.from(json.decode(str).map((x) => MyTeam.fromJson(x)));

String myTeamToJson(List<MyTeam> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MyTeam extends JsonSerializable {
  MyTeam({
    required this.date,
    required this.level,
    required this.members,
    required this.name,
    required this.sport,
  });

  Timestamp date;
  String level;
  String members;
  String name;
  String sport;

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'date': date,
      'level': level,
      'members': members,
      'name': name,
      'sport': sport,
    };
  }

  MyTeam.fromFirebaseMap(Map<String, Object?> data)
      : date = data['date'] as Timestamp,
        level = data['level'] as String,
        members = data['members'] as String,
        name = data['name'] as String,
        sport = data['sport'] as String;

  factory MyTeam.fromJson(Map<String, dynamic> json) => MyTeam(
        date: json["date"],
        level: json["level"],
        members: json["members"],
        name: json["name"],
        sport: json["sport"],
      );

  @override
  Map<String, dynamic> toJson() => {
        "date": date,
        "level": level,
        "members": members,
        "name": name,
        "sport": sport,
      };
}
