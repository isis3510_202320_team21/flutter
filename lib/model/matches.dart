import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

// ignore: non_constant_identifier_names
List<MyMatches> MyMatchesFromJson(String str) =>
    List<MyMatches>.from(json.decode(str).map((x) => MyMatches.fromJson(x)));

// ignore: non_constant_identifier_names
String MyMatchesToJson(List<MyMatches> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MyMatches extends JsonSerializable {
  MyMatches({
    required this.date,
    required this.place,
    required this.result,
    required this.team,
  });

  String date;
  String place;
  String result;
  String team;

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'date': date,
      'place': place,
      'result': result,
      'team': team,
    };
  }

  MyMatches.fromFirebaseMap(Map<String, Object?> data)
      : date = data['date'] as String,
        place = data['place'] as String,
        result = data['result'] as String,
        team = data['team'] as String;

  factory MyMatches.fromJson(Map<String, dynamic> json) => MyMatches(
        date: json["date"],
        place: json["place"],
        result: json["result"],
        team: json["team"],
      );

  @override
  Map<String, dynamic> toJson() => {
        "date": date,
        "place": place,
        "result": result,
        "team": team,
      };
}
