import 'dart:convert';

List<MyTournament> myTournamentFromJson(String str) => List<MyTournament>.from(json.decode(str).map((x) => MyTournament.fromJson(x)));

String myTournamentToJson(List<MyTournament> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MyTournament {
  MyTournament({
    required this.name,
    required this.teams,
    required this.sport,
  });

  String name;
  int teams;
  String sport;

  Map<String, Object?> toFirebaseMap({String? newImage}) {
    return <String, Object?>{
      'name': name,
      'teams': teams,
      'sport': sport,
    };
  }

  MyTournament.fromFirebaseMap(Map<String, Object?> data)
      : name = data['name'] as String,
        teams = data['teams'] as int,
        sport = data['sport'] as String;


  factory MyTournament.fromJson(Map<String, dynamic> json) => MyTournament(
    name: json["name"], 
    teams: json["teams"], 
    sport: json["sport"]
    );

  Map<String, dynamic> toJson() => {
    "name": name,
    "teams": teams,
    "sport": sport,
  };
}
