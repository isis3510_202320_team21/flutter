import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

// ignore: non_constant_identifier_names
List<MyPlaces> MyPlacesFromJson(String str) =>
    List<MyPlaces>.from(json.decode(str).map((x) => MyPlaces.fromJson(x)));

// ignore: non_constant_identifier_names
String MyPlacesToJson(List<MyPlaces> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MyPlaces extends JsonSerializable {
  MyPlaces({
    required this.location,
  });

  String location;

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'location': location,
    };
  }

  MyPlaces.fromFirebaseMap(Map<String, Object?> data)
      : location = data['location'] as String;

  factory MyPlaces.fromJson(Map<String, dynamic> json) => MyPlaces(
        location: json["location"],
      );

  @override
  Map<String, dynamic> toJson() => {
        "location": location,
      };
}
