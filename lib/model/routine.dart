class MyRoutine {
  MyRoutine(
      {required this.name,
      required this.description,
      required this.creationDate,
      required this.category,
      required this.muscles,
      required this.musclesSecondary,
      required this.equipment,
      required this.images,
      required this.comments,
      required this.author,
      required this.rate,
      required this.amount});

  String name;
  String description;
  String creationDate;
  String category;
  List<String> muscles;
  List<String> musclesSecondary;
  List<String> equipment;
  List<String> images;
  List<String> comments;
  String author;
  int rate;
  int amount;

  Map<String, Object?> toFirebaseMap() {
    String equip = "";
    if (equipment.isNotEmpty) {
      equip = equipment.toString().replaceAll("[", "").replaceAll("]", "");
    }
    String musc = "";
    if (muscles.isNotEmpty) {
      musc = muscles.toString().replaceAll("[", "").replaceAll("]", "");
    }
    String musc2 = "";
    if (musclesSecondary.isNotEmpty) {
      musc2 =
          musclesSecondary.toString().replaceAll("[", "").replaceAll("]", "");
    }
    String img = "https://wger.de/media/exercise-images/91/Crunches-1.png";
    if (images.isNotEmpty) {
      img = images.first;
    }
    return <String, Object?>{
      "amount": amount,
      "author": author,
      "category": category,
      "creationDate": creationDate,
      "description": description,
      "equipment": equip,
      "image": img,
      "muscles": musc,
      "name": name,
      "rate": rate,
      "secondaryMuscles": musc2
    };
  }
}
