import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

List<MySport> mySportFromJson(String str) =>
    List<MySport>.from(json.decode(str).map((x) => MySport.fromJson(x)));

String mySportToJson(List<MySport> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MySport extends JsonSerializable {
  MySport({
    required this.name,
    required this.players,
    required this.rules,
  });

  String name;
  int players;
  String rules;

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'name': name,
      'players': players,
      'rules': rules,
    };
  }

  MySport.fromFirebaseMap(Map<String, Object?> data)
      : name = data['name'] as String,
        players = data['players'] as int,
        rules = data['rules'] as String;

  factory MySport.fromJson(Map<String, dynamic> json) => MySport(
        name: json["name"],
        players: json["players"],
        rules: json["rules"],
      );

  @override
  Map<String, dynamic> toJson() => {
        "name": name,
        "players": players,
        "rules": rules,
      };
}
