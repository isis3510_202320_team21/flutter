class Weather {
  final double? temp;
  final double? feelsLike;
  final String? description;
  final String? icon;
  final String? main;

  Weather({
    this.temp,
    this.feelsLike,
    this.description,
    this.icon,
    this.main,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      temp: json['main']['temp'].toDouble(),
      feelsLike: json['main']['feels_like'].toDouble(),
      description: json['weather'][0]['description'],
      icon: json['weather'][0]['icon'],
      main: json['weather'][0]['main'],
    );
  }
}
